/** @file
 * This file is part of lppl.
 * Copyright David Rushing Dewhurst, 2022 - present.
 * Released under MIT license
 */

#ifndef DISTRIBUTIONS_H
#define DISTRIBUTIONS_H

#define _USE_MATH_DEFINES

#include <iostream>
#include <limits>
#include <sstream>
#include <string>
#include <tuple>
#include <utility>
#include <variant>
#include <vector>
#include <random>

#include <domains.hpp>
#include <functional_util.hpp>

namespace Distributions {

    // distribution traits

    template<typename D>
    struct arity {
        static constexpr size_t value = 0;
    };

    template<typename D>
    struct input_types {
        using value = std::tuple<>;
    };

    template<typename D>
    struct output_dim {
        static constexpr size_t value = 1;
    };

    template<typename D>
    struct output_domain;

    // parameters

    template<typename V>
    struct Parameter {
        using type = typename V::type;
        type value;

        Parameter(type value) : value(value) {}

        template<typename RNG>
        type sample(RNG&) { return value; }

        void set_value(type value_) { value = value_; }

        double logprob(type value_) {
            return (value_ == value) ? 0.0 : -std::numeric_limits<double>::infinity();
        }

        std::string string() const {
            return "Parameter(value=" + stringify(value) + ")";
        }
    };

    template<typename V>
    struct output_domain<Parameter<V>> {
        using value = V;
    };

    template<typename V> 
    struct arity<Parameter<V>> {
        static constexpr size_t value = 1;
    };

    template<typename V>
    struct input_types<Parameter<V>> {
        using value = std::tuple<typename Parameter<V>::type>;
    };

    // actual distributions

    /**
     * @brief A discrete uniform distribution over integers.
     * 
     */
    struct DiscreteUniform {

        int low;
        int high;
        std::uniform_int_distribution<> dist;
           
        DiscreteUniform(int low, int high) 
            : low(low), high(high), dist(std::uniform_int_distribution(low, high)) {}
        DiscreteUniform(int high)
            : DiscreteUniform(0, high) {}
        DiscreteUniform()
            : DiscreteUniform(0, 1) {}

        template<typename... Vs>
        DiscreteUniform(std::variant<Vs...> low, std::variant<Vs...> high)
            : DiscreteUniform(std::get<int>(low), std::get<int>(high)) {}

        template<typename RNG>
        int sample(RNG& rng) { return dist(rng); }

        double logprob(int value) const {
            return -std::log(high - low + 1);
        }
        std::string string() {
            return "DiscreteUniform(low=" + std::to_string(low) + ", high=" + std::to_string(high) + ")";
        }

    };

    /// \note: DiscreteUniform has no output domain defined!

    template<> struct arity<DiscreteUniform> { static constexpr size_t value = 2; };
    template<> struct input_types<DiscreteUniform> { using value = std::tuple<int, int>; };

   /**
     * @brief A continuous uniform distribution over doubles.
     * 
     */
    struct Uniform  {
        double low;
        double high;
        std::uniform_real_distribution<> dist;

        Uniform(double low, double high)
            : low(low), high(high), dist(std::uniform_real_distribution(low, high)) {}
        Uniform(double high)
            : Uniform(0.0, high) {}
        Uniform()
            : Uniform(0.0, 1.0) {}

        template<typename... Vs>
        Uniform(std::variant<Vs...> low, std::variant<Vs...> high)
            : Uniform(std::get<double>(low), std::get<double>(high)) {}

        template<typename RNG>
        double sample(RNG& rng) { return dist(rng); }

        double logprob(double value) const {
            return -std::log(high - low);
        }
        std::string string() const {
            return "Uniform(low=" + std::to_string(low) + ", high=" + std::to_string(high) + ")";
        }
    };

    /// \note: Uniform has no output domain defined!

    template<> struct arity<Uniform> { static constexpr size_t value = 2; };
    template<> struct input_types<Uniform> { using value = std::tuple<double, double>; };

    /**
     * A normal distribution parameterized by location and scale.
     */
    struct Normal {

        double loc;
        double scale;
        std::normal_distribution<> dist;

        Normal(double loc, double scale) 
            : loc(loc),
              scale(scale), 
              dist(std::normal_distribution(loc, scale)) {}

        template<typename... Vs>
        Normal(std::variant<Vs...> loc, std::variant<Vs...> scale)
            : Normal(std::get<double>(loc), std::get<double>(scale)) {}

        Normal(double loc)
            : Normal(loc, 1.0) {}
        Normal()
            : Normal(0.0, 1.0) {}

        std::string string() const {
            return "Normal(loc=" + std::to_string(loc) + ", scale=" + std::to_string(scale) + ")";
        }

        template<typename RNG>
        double sample(RNG& rng) { return dist(rng); }

        /**
         * The log probability of the value under the distribution. 
         * 
         * @param value the value to score
         * @return double the log probability of the value under N(loc, scale)
         */ 
        double logprob(double value) const {
            return -0.5 * std::pow((value - loc) / scale, 2.0) - (0.5 * std::log(2.0 * M_PI) + std::log(scale));
        }
    };

    template<> struct output_domain<Normal> { using value = unbounded<double>; };
    template<> struct arity<Normal> { static constexpr size_t value = 2; };
    template<> struct input_types<Normal> { using value = std::tuple<double, double>; };

    /**
     * A dynamic categorical distribution parameterized by a std::vector<double> of probabilities
     */
    struct Categorical {
            
        std::vector<double> probs;
        std::discrete_distribution<int> dist;

        Categorical(std::vector<double> probs)
            : probs(probs), 
              dist(std::discrete_distribution<int>(probs.begin(), probs.end())) {}

        template<typename... Vs>
        Categorical(std::variant<Vs...> probs)
            : Categorical(std::get<std::vector<double>>(probs)) {}

        Categorical(int dim)
            : Categorical(_to_uniform_vector(dim)) {}

        Categorical()
            : Categorical(2) {}

        /**
         * @brief Get the probability at the ix-th index
         * 
         */
        double at(size_t ix) { return probs[ix]; }

        std::vector<double> get_probs() { return probs; }

        unsigned long get_dim() { return probs.size(); }

        std::string string() const {
            std::stringstream ss;
            ss << "[";
            for (size_t i = 0; i != probs.size(); i++) {
                if (i != 0) {
                    ss << ",";
                }
                ss << probs[i];
            }
            ss << "]";
            std::string str_probs = ss.str();
            return "Categorical(probs=" + str_probs + ")";
        }

        double logprob(unsigned long value) const {
                return std::log(probs[value]);
        }

        template<typename RNG>
        unsigned long sample(RNG& rng) { return dist(rng); }
    };

    /// \note: Categorical has no output domain defined!

    template<> struct arity<Categorical> { static constexpr size_t value = 1; };
    template<> struct input_types<Categorical> { using value = std::tuple<std::vector<double>>; };

    /**
     * A gamma distribution parameterized by shape k and scale theta.
     */
    struct Gamma {
        double k;
        double theta;
        std::gamma_distribution<double> dist;
        
        Gamma(double k, double theta)
            : k(k), theta(theta), dist(std::gamma_distribution<double>(k, theta)) {}

        template<typename... Vs>
        Gamma(std::variant<Vs...> k, std::variant<Vs...> theta)
            : Gamma(std::get<double>(k), std::get<double>(theta)) {}

            /**
             * A gamma distribution with k = 1; corresponds to exponential distribution with scale = theta
             *
             * @param theta scale of the distribution; rate = 1/scale.
             */
            Gamma(double theta)
                : Gamma(1.0, theta) {}

            /**
             * A gamma distribution with k = theta = 1; corresponds to a standard exponential distribution.
             */
            Gamma() : Gamma(1.0, 1.0) {}

            /**
             * Get a std::string representation of the categorical distribution.
             */
            std::string string() const {
                return "Gamma(k=" + std::to_string(k) + ", theta=" + std::to_string(theta) + ")";
            }

            double logprob(double value) const {
                return (k - 1.0) * std::log(value) - value / theta - (k * std::log(theta) + lgamma(k));
            }

            template<typename RNG>
            double sample(RNG& rng) { return dist(rng); }
    };

    template<> struct output_domain<Gamma> { using value = non_negative<double>; };
    template<> struct arity<Gamma> { static constexpr size_t value = 2; };
    template<> struct input_types<Gamma> { using value = std::tuple<double, double>; };

    /**
     * @brief A beta distribution parameterized by shape parameters alpha and beta.
     * 
     */
    struct Beta {

        double alpha;
        double beta;
        Gamma alpha_dist;
        Gamma beta_dist;

        Beta(double alpha, double beta)
            : alpha(alpha),
              beta(beta),
              alpha_dist(Gamma(alpha, 1.0)),
              beta_dist(Gamma(beta, 1.0)) {}

        template<typename... Vs>
        Beta(std::variant<Vs...> alpha, std::variant<Vs...> beta)
            : Beta(std::get<double>(alpha), std::get<double>(beta)) {}

        Beta(double beta) : Beta(1.0, beta) {}

        std::string string() const {
            return "Beta(alpha=" + std::to_string(alpha) + ", beta=" + std::to_string(beta) + ")";
        }

        template<typename RNG>
        double sample(RNG& rng) {
            double x = alpha_dist.sample(rng);
            double y = beta_dist.sample(rng);
            return x / (x + y);
        }
        
        double logprob(double value) const {
            return (alpha - 1) * std::log(value) + (beta - 1) * std::log(1 - value) + std::lgamma(alpha + beta) - std::lgamma(alpha) - std::lgamma(beta);
        }

    };  // class Beta

    template<> struct output_domain<Beta> { using value = unit_interval<double>; };
    template<> struct arity<Beta> { static constexpr size_t value = 2; };
    template<> struct input_types<Beta> { using value = std::tuple<double, double>; };

    struct Poisson {
        double rate;
        std::poisson_distribution<unsigned> dist;

        public:
            Poisson(double rate) 
                : rate(rate), dist(std::poisson_distribution<unsigned>(rate)) {}

            template<typename... Vs>
            Poisson(std::variant<Vs...> rate)
                : Poisson(std::get<double>(rate)) {}

            Poisson() : Poisson(1.0) {}

            std::string string() const {
                return "Poisson(rate=" + std::to_string(rate) + ")";
            }

            double logprob(unsigned value) const {
                return -rate + value * std::log(rate) - lgamma(value + 1.0);
            }

            template<typename RNG>
            unsigned sample(RNG& rng) { return dist(rng); }

    };  // class Poisson

    template<> struct output_domain<Poisson> { using value = non_negative<unsigned>; };
    template<> struct arity<Poisson> { static constexpr size_t value = 1; };
    template<> struct input_types<Poisson> { using value = std::tuple<double>; };

}  // namespace Distributions

#endif  // DISTRIBUTIONS_H
