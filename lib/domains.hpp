/**  
 * @file
 * This file is part of lppl.
 * Copyright David Rushing Dewhurst, 2023 - present.
 * Released under MIT license
 */

// basic one-dimensional domains

#include <math.h>

#ifndef DOMAINS_H
#define DOMAINS_H

template<typename T>
struct unbounded {
    using type = T;
};

template<typename T>
struct non_negative {
    using type = T;
    constexpr static T lower_bound = 0;
};

/// \note Maybe bounded will return in later versions? Not well thought-out.

// template<typename T, T lower, T upper>
// struct bounded {
//     using type = T;
//     constexpr static T lower_bound = lower;
//     constexpr static T upper_bound = upper;
// };

template<typename T>
struct unit_interval;

template<>
struct unit_interval<double> {
    using type = double;
};

// mappings between them

template<typename From, typename To>
struct mapping;

template<template<typename> class A, typename T>
struct mapping<A<T>, A<T>> {
    static T forward(T& x) { return x; }
    static T forward(T&& x) { return forward(x); }
    static T backward(T& x) { return x; }
    static T backward(T&& x) { return backward(x); }
};

template<typename T>
struct mapping<unbounded<T>, non_negative<T>> {
    static T forward(T& x) { 
        return (T) x < 20.0 ? std::log(1.0 + std::exp(x)) : x;
    }
    static T forward(T&& x) { return forward(x); }
    static T backward(T& x) {
        return (T) x < 20.0 ? std::log(std::exp(x) - 1.0) : x;
    }
    static T backward(T&& x) { return backward(x); }
};

template<>
struct mapping<unbounded<double>, unit_interval<double>> {
    static double forward(double& x) {
        return 1.0 / (1.0 + std::exp(-x));
    }
    static double forward(double&& x) { return forward(x); }
    static double backward(double& x) {
        return std::log(x) - std::log(1.0 - x);
    }
    static double backward(double&& x) { return backward(x); }
};

template<typename A, typename B>
struct mapping<B, A> {
    template<typename T> static T forward(T& x) { return mapping<A, B>::backward(x); }
    template<typename T> static T forward(T&& x) { return mapping<A, B>::backward(x); }
    template<typename T> static T backward(T& x) { return mapping<A, B>::forward(x); }
    template<typename T> static T backward(T&& x) { return mapping<A, B>::forward(x); }
};

#endif  // DOMAINS_H