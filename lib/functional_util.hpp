/* 
 * This file is part of lppl.
 * Copyright David Rushing Dewhurst, 2022 - present.
 * Released under MIT license
 */


#ifndef FUNCTIONAL_UTIL_H
#define FUNCTIONAL_UTIL_H


#include <array>
#include <memory>
#include <string>
#include <tuple>
#include <type_traits>
#include <variant>
#include <vector>


template<typename T, typename... Ts>
struct cat;

template<typename... Ts, typename... Us, typename... Vs>
struct cat<std::variant<Ts...>, std::variant<Us...>, Vs...> 
    : cat<std::variant<Ts..., Us...>, Vs...> {};

template<typename... Ts, typename... Us>
struct cat<std::variant<Ts...>, Us...> {
    using type = std::variant<Ts..., Us...>;
};

std::vector<double> _to_uniform_vector(int dim) {
    std::vector<double> probs;
    for (int i = 0; i != dim; i++) {
        probs.push_back(1.0 / dim);
    }
    return probs;
}


template<typename... Ts>
struct overloaded : Ts... {
    using Ts::operator()...;
};
template<class... Ts> overloaded(Ts...) -> overloaded<Ts...>;

template<typename T, typename...>
std::string stringify(T t) {
    return std::to_string(t);
}

template<typename T>
std::string stringify(std::vector<T> t) {
    std::string out = "[ ";
    for (size_t ix = 0; ix != t.size(); ix++) {
        out += stringify(t[ix]) + " ";
    }
    out += "]";
    return out;
}

template<typename T, size_t N>
std::string stringify(std::array<T, N> t) {
    std::string out = "[ ";
    for (size_t ix = 0; ix != N; ix++) {
        out += stringify(t[ix]) + " ";
    }
    out += "]";
    return out;
}

template<typename T>
std::string stringify(std::shared_ptr<T> t) {
    return "(shared ptr) " + stringify(*t);
}

// Piotr Skotnicki's implementation

template <typename T, typename... Ts>
struct filter_duplicates { using type = T; };

template <template <typename...> class C, typename... Ts, typename U, typename... Us>
struct filter_duplicates<C<Ts...>, U, Us...>
    : std::conditional_t<(std::is_same_v<U, Ts> || ...)
                       , filter_duplicates<C<Ts...>, Us...>
                       , filter_duplicates<C<Ts..., U>, Us...>> {};

template <typename T>
struct unique_variant;

template <typename... Ts>
struct unique_variant<std::variant<Ts...>> : filter_duplicates<std::variant<>, Ts...> {};

template <typename T>
using unique_variant_t = typename unique_variant<T>::type;

// end Piotr Skotnicki's implementation

// artyer's implementation

template<std::size_t... I, typename F>
constexpr void call_with_range(F&& f, std::index_sequence<I...>) {
    ((f(std::integral_constant<std::size_t, I>{})), ...);
}

template<std::size_t N, typename F>
constexpr void call_with_range(F&& f) {
    call_with_range(f, std::make_index_sequence<N>{});
}

template<typename T, typename F>
constexpr void call_with_range(F&& f) {
    call_with_range<std::tuple_size_v<T>>(f);
}

// end artyer's implementation

#endif  // FUNCTIONAL_UTIL_H