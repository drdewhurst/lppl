/**  
 * @file
 * This file is part of lppl.
 * Copyright David Rushing Dewhurst, 2022 - present.
 * Released under MIT license
 */

#ifndef SCORE_H
#define SCORE_H

#include <functional>
#include <variant>

#include <record.hpp>

/**
 * @brief A node denoting the result of evaluating a black-box score function
 * 
 * @tparam I the input type to the score function. A score function outputs a double score
 *  which is to be maximized.
 */
template<typename I>
struct Score {
    std::function<double(I)> f;

    Score(std::function<double(I)> f) : f(f) {};

    template<typename RNG>
    double sample(RNG&) {
        return 0.0;
    }

    /**
     * @brief Evaluates the score function.
     * 
     * @param input 
     * @return double 
     */
    double logprob(I input) {
        return f(input);
    }

    std::string string() const {
        return "Score<I>(...)";
    }
};

/**
 * @brief Evaluates a black-box score function.
 * 
 * Interprets the score as incrementing log probability and log likelihood of the trace.
 * This can be used to define a non-normalized energy function for MCMC algorithms or to
 * conduct stochastic optimization.
 * 
 * @tparam I the input type to the score function
 * @tparam Ts the types in the associated record
 * @param r the record in which the result will be stored
 * @param address the address at which the result will be stored
 * @param f the score function to be evaluated
 * @param input the value with which to evaluate the score function
 * @return double the score value
 */
template<typename I, typename... Ts>
double score(record_t<Ts...>& r, std::string address, std::function<double(I)>& f, I input) {
    observe(r, address, Score<I>(f), input);
    return std::visit([](auto& n) -> double {return n.logprob;}, r.map[address]);
}

#endif  // SCORE_H
