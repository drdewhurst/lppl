/** 
 * @file 
 * This file is part of lppl.
 * Copyright David Rushing Dewhurst, 2022 - present.
 * Released under MIT license
 */

#ifndef PLATE_H
#define PLATE_H

#include <array>
#include <string>
#include <type_traits>

#include <distributions.hpp>
#include <record.hpp>

template<typename D>
struct plate_base_ {
    D dist;

    template<typename... Ts>
    plate_base_(Ts... ts) : dist(D(ts...)) {}
};

template<typename D, size_t N>
struct static_plate : plate_base_<D> {

    template<typename... Ts>
    static_plate(Ts... ts) : plate_base_<D>(ts...) {}

    template<typename RNG>
    std::shared_ptr<std::array<DSType<D>, N>>
    sample(RNG& rng) {
        auto out = std::make_shared<std::array<DSType<D>, N>>();
        for (size_t ix = 0; ix != N; ix++) {
            out->operator[](ix) = this->dist.sample(rng);
        }
        return out;
    }

    double logprob(std::shared_ptr<std::array<DSType<D>, N>> value) {
        double out = 0.0;
        for (size_t ix = 0; ix != N; ix++) out += this->dist.logprob(value->operator[](ix));
        return out;
    }

    std::string string() const {
        return "static_plate<" + this->dist.string() + ", " + std::to_string(N) + ">";
    }

};

#endif  // PLATE_H