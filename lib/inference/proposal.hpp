/** 
 * \file proposal.hpp  
 * This file is part of lppl.
 * Copyright David Rushing Dewhurst, 2022 - present.
 * Released under MIT license
 */

#ifndef PROPOSAL_H
#define PROPOSAL_H

#include <iostream>

#include <utility>
#include <variant>

#include <functional_util.hpp>
#include <record.hpp>
#include <query.hpp>

/**
 * @brief Abstract base class from which all proposals should inherit.
 * 
 * Subclasses must implement a single method, generate, that takes no
 * arguments and returns a record of distribution types Out...
 * 
 * @tparam Out 
 */
template<
    class Impl, 
    typename... Out
>
struct proposal {

    std::pair<record_t<Out...>, double> generate() {
        return static_cast<Impl*>(this)->generate();
    }

    std::pair<record_t<Out...>, double> operator()() {
        auto r = generate();
        r.first.interp.push_front(record_interp_t::replay);
        return r;
    }

};

/**
 * @brief Type of values returned from filtering algorithms
 * 
 * @tparam O The output type of a probabilistic program
 * @tparam Ts The types of the distributions in the probabilistic program
 */
template<typename O, typename... Ts>
using FilterValueType = std::shared_ptr<record_collection_t<O, Ts...>>;

/**
 * @brief Abstract user-defined transition dynamics for use in filtering (or elsewhere!)
 * 
 * @tparam Impl The implementation of the transition kernel logic.
 * @tparam O the output type of the probabilistic program
 * @tparam Out The types of distributions in the records generated by the subclass
 */
template<
    class Impl, 
    typename O, 
    typename... Out
>
struct transition {

    void update(FilterValueType<O, Out...>& r) {
        return static_cast<Impl*>(this)->update(r);
    }

    std::pair<record_t<Out...>, double> generate() {
        return static_cast<Impl*>(this)->generate();
    }

    std::pair<record_t<Out...>, double> operator()() {
        auto r = generate();
        r.first.interp.push_front(record_interp_t::replay);
        return r;
    }


};

/**
 * @brief Creates a bare node of type node_t<Out> from a record of nearly arbitrary type.
 * 
 * The node in the input record at the specified address must have a value type equal to 
 * the value type generated by Out.
 * 
 * @tparam Out the underlying distribution in the output node
 * @tparam In the collection of distributions by which the input record is parameterized
 * @param r_in the input record
 * @param address the address in the input record to access
 * @return node_t<Out> the created bare node
 */
template<typename Out, typename... In>
node_t<Out> extract(record_t<In...>& r_in, std::string address) {
    using ThisT = decltype(std::declval<Out>().sample(__COMPILERNG__));
    auto value = std::get<ThisT>(
        std::visit(
            [](auto& node) -> unique_variant_t<var_ret_t<In...>> { return node.value; },
            r_in.map[address]
        )
    );
    return node_t<Out> {
        .address=address,
        .value=value,
        .obs=false,
        .interp=interp_t::replay,
        .last_interp=interp_t::standard
    };
}

bool in(std::string a, std::vector<std::string> vec) {
    for (auto& v : vec) {
        if (a == v) return true;
    }
    return false;
}

/**
 * @brief Transfers a newly created node into a record of a compatible type.
 * 
 * @tparam D the underlying distribution type of the created node
 * @tparam Out the parameterization of the record to which the transfer occurs
 * @param r_out the record to which the transfer occurs
 * @param n the node to be transferred
 * @return record_t<Out...>& a reference to the record to which the transfer occurred
 */
template<typename D, typename... Out>
void transfer(record_t<Out...>& r_out, node_t<D>&& n) {
    r_out.map.insert_or_assign(n.address, n);
    if (!in(n.address, r_out.addresses)) r_out.addresses.push_back(n.address);
}

/**
 * @brief Propose a value from distribution of type P into a model node of type D.
 * 
 * This function is intended for use in proposal subclasses only. 
 * 
 * @tparam D the type of the distribution node in which to store the value
 * @tparam P the type from which to sample and score the value
 * @tparam RNG the type of PRNG used
 * @tparam Ts the parameterization of the record
 * @param r the record in which to store a node_t<D>
 * @param address the address at which to store the node
 * @param dist the distribution from which to sample and score the value; the proposal distribution
 * @param rng the PRNG used
 * @return auto the sampled value
 */
template<typename D, typename P, typename RNG, typename... Ts>
auto propose(record_t<Ts...>& r, std::string address, P&& dist, RNG& rng) {
    auto value = dist.sample(rng);
    auto logprob = dist.logprob(value);
    transfer(
        r,
        (node_t<D>) {
            .address=address,
            .value=value,
            .logprob=logprob,
            .obs=false,
            .interp=interp_t::replay,
            .last_interp=interp_t::standard
        }
    );
    return value;
}

#endif  // PROPOSAL_H
