/** 
 * \file inference.hpp
 * This file is part of lppl.
 * Copyright David Rushing Dewhurst, 2022 - present.
 * Released under MIT license
 */

#ifndef INFERENCE_H
#define INFERENCE_H

#include <type_traits>

#include <effects.hpp>
#include <record.hpp>
#include <query.hpp>

/**
 * @brief What kind of kernel is it (i.e., does it not exist at all, does it propose to only some
 * of the address space, or does it propose to the entire address space?)
 * 
 */
enum class kernel {none, partial, full};

/**
 * @brief Will the passed class template have an associated proposal distribution?
 * 
 * @tparam C The class template to check
 */
template<template<class, class, class, template<class, class, class...> class, typename...> class C>
struct has_proposal {
    static constexpr kernel value = kernel::none;
};

/**
 * @brief Universal base class for inference methods. 
 * 
 * @tparam A The class template of the inference method
 * @tparam I The type of the input data
 * @tparam O The type of the output data
 * @tparam V The type of the query variable(s)
 * @tparam Q The class template of the queryer
 * @tparam HasProposal will the passed class template have an associated proposal
 *  class?
 * @tparam Ts The distribution types in the program
 */
template<
    template<
        typename,
        typename,
        typename,
        template<class, class, class...> class,
        typename...
    > class A,
    typename I,
    typename O,
    typename V,
    template<class, class, class...> class Q,
    typename... Ts
>
struct Inference {
    pp_t<I, O, Ts...> f;
    Q<V, O, Ts...>& queryer;
    inf_options_t opts; 

    Inference(
        pp_t<I, O, Ts...> f,
        Q<V, O, Ts...>& queryer,
        inf_options_t opts
    ) : f(f), queryer(queryer), opts(opts) {}

    /**
     * @brief Runs the specified inference algorithm with the specified queryer using
     *  the specified proposal distribution.
     * 
     * This operator exists only for inference algorithms that require a full proposal distribution (e.g.,
     * importance sampling)
     * 
     * @tparam P The type of the proposal distribution
     * @param input the input data to the probabilistic program
     * @param proposal the proposal distribution
     * @return V The return value of the queryer
     */
    template<
        class P, 
        template<
            typename,
            typename,
            typename,
            template<class, class, class...> class,
            typename...
        > class G = A
    >
    std::enable_if_t<has_proposal<G>::value == kernel::full, V> operator()(I& input, P& proposal) {
        return static_cast<A<I, O, V, Q, Ts...>*>(this)->operator()(input, proposal);
    }

    /**
     * @brief Runs the specified inference algorithm with the specified queryer using
     *  the specified proposal distribution.
     * 
     * This operator exists only for inference algorithms that require a partial proposal distribution (e.g.,
     * generic metropolis-hastings)
     * 
     * @param input the input data to the probabilistic program
     * @param proposal the proposal distribution
     * @return V The return value of the queryer
     */
    template<
        template<
            typename,
            typename,
            typename,
            template<class, class, class...> class,
            typename...
        > class G = A
    >
    std::enable_if_t<has_proposal<G>::value == kernel::partial, V> operator()(I& input, pp_t<record_t<Ts...>, double, Ts...>& proposal) {
        return static_cast<A<I, O, V, Q, Ts...>*>(this)->operator()(input, proposal);
    }

    /**
     * @brief Runs the specified inference algorithm with the specified queryer.
     * 
     * This operator exists only for inference algorithms that do not require a proposal distribution.
     * 
     * @param input the input data to the probabilistic program
     * @return V The return value of the queryer
     */
    template<
        template<
            typename,
            typename,
            typename,
            template<class, class, class...> class,
            typename...
        > class G = A
    >
    std::enable_if_t<has_proposal<G>::value == kernel::none, V> operator()(I& input) {
        return static_cast<A<I, O, V, Q, Ts...>*>(this)->operator()(input);
    }

};

/**
 * @brief Factory function for Inference instances
 * 
 */
template<
    template<
        typename,
        typename,
        typename,
        template<class, class, class...> class Q,
        typename...
    > class A,
    typename I,
    typename O,
    typename V,
    template<class, class, class...> class Q,
    typename... Ts
>
Inference<A, I, O, V, Q, Ts...>
inference(pp_t<I, O, Ts...> f, Q<V, O, Ts...>& queryer, inf_options_t opts) {
    return Inference<A, I, O, V, Q, Ts...>(f, queryer, opts);
}

#endif  // INFERENCE_H
