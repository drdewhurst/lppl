/** 
 * \file kernels.hpp
 * This file is part of lppl.
 * Copyright David Rushing Dewhurst, 2023 - present.
 * Released under MIT license
 */

#ifndef FILTER_KERNELS_H
#define FILTER_KERNELS_H

#include <distributions.hpp>

namespace transition_kernels {

/**
 * @brief A generic double-valued random walk transition kernel.
 * 
 * This kernel assumes the distribution to be superceded \f$p\f$ is in the location-scale family.
 * Denote the random variable by \f$z\f$.
 * Given a user-defined scale \f$\sigma\f$ and an *unweighted* cache \f$D_{t-1}(z)\f$ of previous samples 
 * of \f$z\f$, this distribution corresponds to 
 * \f$z \sim p\left( E_{z \sim D_{t-1}(z)}[z], \sigma \right) \f$.
 * 
 * @tparam D The type of distribution to be used
 * @tparam Ts The types of distributions in the record
 */
template<typename D, typename... Ts>
struct rw_trans_kernel {
    double loc;
    double scale;
    std::string address;

    /**
     * @brief Construct a new rw trans kernel object
     * 
     * @param address the address to be modeled
     * @param loc the initial value of the location parameter
     * @param scale the value of the scale parameter
     */
    rw_trans_kernel(std::string address, double loc, double scale)
        : loc(loc), scale(scale), address(address) {}

    /**
     * @brief Construct a new rw trans kernel object. Sets loc = 0.0, scale = 1.0.
     * 
     * @param address the address to be modeled.
     */
    rw_trans_kernel(std::string address) 
        : rw_trans_kernel(address, 0.0, 1.0) {}

    void update(FilterValueType<double, Ts...>& cache) { loc = mean(cache, address); }

    template<typename RNG>
    void generate(record_t<Ts...>& r, RNG& rng) {
        sample(r, address, D(loc, scale), rng);
    }

};

/**
 * @brief Specialization of rw_trans_kernel for gamma distributions.
 * 
 * The gamma distribution in \f$(k, \theta)\f$ is transformed to (location/scale) = 
 * \f$(\mu, \sigma)\f$ form by the following equations:
 * \f$k = (\mu/\sigma)^2;\ \theta = \sigma^2 / \mu\f$.
 * 
 * \todo finish description of update equations
 * 
 * @tparam Ts The types of distributions in the probabilistic program
 */
template<typename... Ts>
struct rw_trans_kernel<Distributions::Gamma, Ts...> {
    double decay;
    double weight;
    double loc;
    double scale;
    double k;
    double theta;
    std::string address;

    void update_internal(double loc, double scale) {
        k = std::pow(loc / scale, 2.0);
        theta = std::pow(scale, 2.0) / loc;
    }

    rw_trans_kernel(std::string address, double decay, double weight, double loc, double scale)
        : decay(decay), weight(weight), loc(loc), scale(scale), address(address) { update_internal(loc, scale); }

    rw_trans_kernel(std::string address, double decay, double weight) 
        : rw_trans_kernel(address, decay, weight, 1.0, 1.0) {}

    rw_trans_kernel(std::string address)
        : rw_trans_kernel(address, 0.0, 0.0, 1.0, 1.0) {}

    static double max(double a, double b) { return (a > b) ? a : b; }

    void update(FilterValueType<double, Ts...>& cache) {
        loc = max((weight * loc + (1.0 - weight) * mean(cache, address)) / (1.0 + decay), 0.1);
        update_internal(loc, max(scale / (1.0 + decay), 0.1));
    }

    template<typename RNG>
    void generate(record_t<Ts...>& r, RNG& rng) {
        sample(r, address, Distributions::Gamma(k, theta), rng);
    }
};

}  // namespace transition_kernels

#endif  // FILTER_KERNELS_H