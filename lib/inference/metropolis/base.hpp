/** 
 * \file 
 * This file is part of lppl.
 * Copyright David Rushing Dewhurst, 2022 - present.
 * Released under MIT license
 */

#ifndef METRO_BASE_H
#define METRO_BASE_H

#include <algorithm>
#include <iostream>
#include <cstddef>
#include <cmath>
#include <functional>
#include <memory>
#include <optional>
#include <random>
#include <unordered_map>
#include <vector>

#include <distributions.hpp>
#include <record.hpp>
#include <effects.hpp>
#include <query.hpp>

#include <inference/inference.hpp>
#include <inference/proposal.hpp>

template<typename... Ts>
struct MetropolisState {
    Uniform u;
    record_t<Ts...> r;
    std::minstd_rand rng;

    MetropolisState(unsigned seed=2022) : rng(seed) {}

    double log_u() {
        return std::log(u.sample(rng));
    }
};

/**
 * @brief Log acceptance probability for use when proposing from the prior
 * 
 * @tparam Ts the distribution types contained in each record
 * @param orig_r the current record
 * @param new_r the proposed new record
 * @return double log acceptance probability
 */
template<typename... Ts>
double log_accept_prior(record_t<Ts...>& orig_r, record_t<Ts...>& new_r) {
    double new_ll = loglikelihood(new_r);
    double old_ll = loglikelihood(orig_r);
    return new_ll - old_ll;
}

/**
 * @brief Metropolis Hastings using the prior distribution as the proposal.
 * 
 * Suppose the generative model factors as \f$ p(x,z) = p(x|z)p(z) \f$.
 * The acceptance ratio is \f$ \log \alpha = \log p(x|z') - \log p(x|z) \f$, where
 * \f$ z' \sim p(z) \f$ is the new draw from the prior, and \f$ z \sim p(z) \f$ is the
 * old/existing draw from the prior. Each weight passed to the queryer is \f$ \log w = 0 \f$.
 * 
 * @tparam I The input type of the program on which to perform inference
 * @tparam O The output type of the program on which to perform inference
 * @tparam V The type emitted by the queryer
 * @tparam Q The queryer type
 * @tparam Ts The distribution types used in the probabilistic program
 */
template<typename I, typename O, typename V, template<class, class, class...> class Q, typename... Ts>
struct AncestorMetropolis : Inference<AncestorMetropolis, I, O, V, Q, Ts...> {
    void step(MetropolisState<Ts...>& state, pp_t<I, O, Ts...> f, Q<V, O, Ts...>& queryer, I input, inf_options_t opts) {
        record_t<Ts...> new_r;
        O o = f(new_r, input);
        double log_accept = log_accept_prior(state.r, new_r);
        if (state.log_u() < log_accept) state.r = new_r;
        if ((opts._num_complete > opts._burn) && (opts._num_complete % opts._thin == 0)) {
            queryer.update(state.r, o, 0.0, opts);
        }
    }
    V operator()(I& input) {
        auto state = MetropolisState<Ts...>(this->opts._seed);
        this->f(state.r, input);
        while (this->opts._num_complete != this->opts._num_iterations) {
            step(state, this->f, this->queryer, input, this->opts);
            this->opts._num_complete++;
        }
        return this->queryer.emit();
    }
};

/**
 * @brief Generic Metropolis-Hastings algorithm with user-specified proposal distribution.
 * 
 * Suppose the probabilistic program factors as \f$p(x, z) = p(x|z)p(z)\f$ and the proposal distribution
 * takes the form \f$q(z'|z)\f$. The log acceptance ratio is computed as
 * \f$ \log \alpha = [\log p(x, z') - \log q(z' | z)] - [\log p(x, z) - \log q(z | z')]\f$, where
 * \f$z' \sim q(z'|z)\f$ are the newly generated unobserved rvs.
 * The proposal distribution need not be symmetric.
 * 
 * @tparam I The input type of the probabilistic program
 * @tparam O The output type of the probabilistic program
 * @tparam V The type of the value to be queried
 * @tparam Q The queryer class template
 * @tparam Ts The types of the distributions in the probabilistic program
 */
template<typename I, typename O, typename V, template<class, class, class...> class Q, typename... Ts>
struct GenericMetropolis : Inference<GenericMetropolis, I, O, V, Q, Ts...> {
    void step(
        MetropolisState<Ts...>& state,
        pp_t<I, O, Ts...>& f,
        Q<V, O, Ts...>& queryer,
        pp_t<record_t<Ts...>, double, Ts...>& proposal,
        pp_t<record_t<Ts...>, double, Ts...>& replayed_proposal,
        I input,
        inf_options_t opts
    ) {
        // log q(z' | z)
        record_t<Ts...> r_new;
        double log_q_zprime_given_z = proposal(r_new, state.r);
        // log p(x, z')
        O o = f(r_new, input);
        double log_p_x_zprime = logprob(r_new);
        // log q(z | z')
        record_t<Ts...> current_r_copy = state.r;
        double log_q_z_given_zprime = replayed_proposal(current_r_copy, r_new);
        // log p(x, z)
        double log_p_x_z = logprob(state.r);

        double log_accept = log_p_x_zprime + log_q_z_given_zprime - log_p_x_z - log_q_zprime_given_z;
        if (state.log_u() < log_accept) state.r = r_new;
        if ((opts._num_complete > opts._burn) && (opts._num_complete % opts._thin == 0)) {
            queryer.update(state.r, o, 0.0, opts);
        }
    }

    /**
     * @brief Runs the inference algorithm
     * 
     * @param input The input to the probabilistic program
     * @param proposal The proposal kernel. It should return \f$log q(z'|z)\f$ i.e. loglatent(new_r).
     * @return V 
     */
    V operator()(I& input, pp_t<record_t<Ts...>, double, Ts...>& proposal) {
        // initialize internal state
        auto state = MetropolisState<Ts...>(this->opts._seed);
        // populate an initial trace
        this->f(state.r, input);
        auto replayed_proposal = replay(proposal);
        auto replayed_f = replay(this->f);

        while (this->opts._num_complete != this->opts._num_iterations) {
            step(state, replayed_f, this->queryer, proposal, replayed_proposal, input, this->opts);
            this->opts._num_complete++;
        }
        return this->queryer.emit();
    }
};

template<>
struct has_proposal<GenericMetropolis> {
    static constexpr kernel value = kernel::partial;
};

#endif  // METRO_BASE_H
