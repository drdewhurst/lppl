/** 
 * \file generic.hpp
 * This file is part of lppl.
 * Copyright David Rushing Dewhurst, 2022 - present.
 * Released under MIT license
 */

#ifndef IS_GENERIC_H
#define IS_GENERIC_H

#include <effects.hpp>
#include <query.hpp>
#include <record.hpp>
#include <inference/inference.hpp>
#include <inference/proposal.hpp>

/**
 * @brief Importance sampling using an arbitrary user-defined proposal distribution.
 * 
 * Suppose the probabilistic program factors as \f$p(x, z) = p(x|z)p(z)\f$, while the proposal distribution
 * is given by \f$q(z|x)\f$. This method proposes values \f$z \sim q(z|x)\f$ and computes weights as
 * \f$ \log w = \log p(x,z) - \log q(z|x)\f$. 
 * 
 * @tparam I The input type of the probabilistic program
 * @tparam O The output type of the probabilistic program
 * @tparam V The type of the address(es) to be queried
 * @tparam Q The class template of the queryer
 * @tparam Ts The distribution types in the probabilistic program
 */
template<typename I, typename O, typename V, template<class, class, class...> class Q, typename... Ts>
struct ImportanceSampling : Inference<ImportanceSampling, I, O, V, Q, Ts...> {
    template<typename P>
    void step(record_t<Ts...>& r, I& input, P& proposal) {
        auto prop = proposal();
        O o = this->f(prop.first, input);
        if (!prop.first.interp.empty()) prop.first.interp.pop_front();
        this->queryer.update(
            prop.first,
            o,
            logprob(prop.first) - prop.second,
            this->opts
        );
    }
    template<typename P>
    V operator()(I& input, P& proposal) {
        record_t<Ts...> r;
        for (size_t n = 0; n != this->opts._num_iterations; n++) step(r, input, proposal);
        return this->queryer.emit();
    }
};

template<>
struct has_proposal<ImportanceSampling> {
    static constexpr kernel value = kernel::full;
};

#endif  // IS_GENERIC_H