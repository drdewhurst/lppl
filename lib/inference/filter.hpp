/** 
 * \file 
 * This file is part of lppl.
 * Copyright David Rushing Dewhurst, 2022 - present.
 * Released under MIT license
 */

#ifndef FILTER_H
#define FILTER_H

#include <optional>
#include <type_traits>

#include <record.hpp>
#include <effects.hpp>
#include <inference/inference.hpp>
#include <inference/proposal.hpp>

#include <query.hpp>

/**
 * @brief The underlying queryer type used by filtering algorithms
 * 
 * @tparam O The output type of the probabilistic program
 * @tparam Ts The types of the distributions in the probabilistic program
 */
template<typename O, typename... Ts>
using FilterQueryerType = WeightedRecord<FilterValueType<O, Ts...>, O, Ts...>;

/**
 * @brief Transforms an arbitrary probabilistic program into a dynamic version.
 * 
 * Given a probabilistic program \f$p(x,z) = p(x|z)p(z)\f$ and a user-supplied
 * transition kernel, converts the model into a dynamic filtering model
 * \f$ p(x,z) = p(x_1 | z_1) p(z_1) \prod_{t\geq 2} p(x_t | z_t)p(z_t|x_{t-1}, z_{t-1})\f$.
 * 
 * @tparam I The input type of the probabilistic program
 * @tparam O The output type of the probabilistic program
 * @tparam RNG The type of the PRNG
 * @tparam Ts The types of the distributions used in the probabilistic program
 * @param f the probabilistic program
 * @param kernel the user-defined transition kernel
 * @param has_been_called has the probabilistic program been called yet?
 * @param rng the PRNG
 * @return pp_t<I, O, Ts...> a new probabilistic program
 */
template<class TransKernel, typename I, typename O, typename RNG, typename... Ts>
pp_t<I, O, Ts...> 
filter_effect(
    pp_t<I, O, Ts...> f, 
    transition<TransKernel, O, Ts...>& kernel, 
    bool& has_been_called,
    RNG& rng
) {
    return pp_t<I, O, Ts...>(
        [f, &kernel, &has_been_called, &rng](record_t<Ts...>& r, I i) -> O {
            // step 0: if this is the initial filtering step, don't do anything dramatic
            if (!has_been_called) {
                return f(r, i);
            }
            // step 1: sample from transition dynamics
            auto this_r_and_logprob = kernel();
            this_r_and_logprob.first.interp.pop_front();
            // step 2: figure out if we require the values from the passed record
            if (r.interp.empty()) {
                // we don't need them
                this_r_and_logprob.first.interp.push_front(record_interp_t::replace);
            } else {
                if (r.interp.front() == record_interp_t::replay) {
                    // this record likely came from a proposal distribution
                    // we do need the values, place them in the transition record
                    // if there are nodes in the passed record not sampled in the transition dynamics...
                    // idk, keep them? \todo figure this out...
                    for (auto& address : r.addresses) {
                        std::visit(
                            [&address, &this_r_and_logprob](auto n){
                                if (this_r_and_logprob.first.map.contains(address)) {
                                    auto value = n.value;
                                    std::visit(
                                        [&address, value](auto& this_n){ this_n.value =  value; },
                                        this_r_and_logprob.first.map[address]
                                    );
                                } else {
                                    // note currently just sticking them in there -- is this right?
                                    this_r_and_logprob.first.map.insert_or_assign(address, n);
                                }
                            },
                            r.map[address]
                        );
                    }
                    this_r_and_logprob.first.interp.push_front(record_interp_t::rewrite);
                } else {
                    // we don't need them because no replay was requested
                    // and originally requesting a rewrite is nonsensical
                    this_r_and_logprob.first.interp.push_front(record_interp_t::replace);
                }
            }
            // step 3: do it!
            r.copy_unobserved_from_other(this_r_and_logprob.first);
            O value = f(r, i);
            if (!r.interp.empty()) r.interp.pop_front();
            return value;
        }
    );
}

/**
 * @brief Computes \f$E_{r(a):\ r \sim D(r)}[f(r(a))]\f$, where \f$D(r)\f$ is the empirical distribution over records
 * and \f$ f: double -> double \f$.
 * 
 * @tparam Ts Types of distributions in the record
 * @param cache the empirical distribution over records
 * @param map_ the mapping function \f$f\f$
 * @param address the address over which to take the mean
 * @return double 
 */
template<typename O, typename... Ts>
double mean(FilterValueType<O, Ts...>& cache, std::function<double(double&&)>&& map_, std::string address) {
    double m = 0.0;
    auto get_value = [](const auto& n) -> double { return n.value; };
    double normalizer = 0.0;
    for (size_t ix = 0; ix != cache->_v.size(); ix++) {
        double weight = cache->prob_at(ix);
        m += map_(std::visit(get_value, cache->_v[ix].map[address])) * weight;
        normalizer += weight;
    }
    return m / normalizer;
}

/**
 * @brief Computes `mean` with \f$f = id \f$.
 * 
 */
template<typename O, typename... Ts>
double mean(FilterValueType<O, Ts...>& cache, std::string address) {
    return mean(cache, [](double&& x) { return x; }, address);
}

template<typename O, typename... Ts>
double variance(FilterValueType<O, Ts...>& cache, std::function<double(double&&)>&& map_, std::string address) {
    double var = 0.0;
    double m = mean(cache, std::forward<decltype(map_)>(map_), address);
    auto get_value = [](const auto& n) -> double { return n.value; };
    for (size_t ix = 0; ix != cache->_v.size(); ix++) {
        var += std::pow(map_(std::visit(get_value, cache->_v[ix].map[address])) - m, 2.0) * cache->prob_at(ix);
    }
    return var;
}

template<typename O, typename... Ts>
double variance(FilterValueType<O, Ts...>& cache, std::string address) {
    return variance(cache, [](double&& x){ return x; }, address);
}

template<typename O, typename... Ts>
double stddev(FilterValueType<O, Ts...>& cache, std::function<double(double&&)>&& map_, std::string address) { 
    return std::sqrt(variance(cache, std::forward<decltype(map_)>(map_), address)); 
}

template<typename O, typename... Ts>
double stddev(FilterValueType<O, Ts...>& cache, std::string address) {
    return stddev(cache, [](double&& x){ return x; }, address);
}

/**
 * @brief Computes \f$dist(r(a)) \sim D(r)\f$, where \f$dist(r(a))\f$ is the distribution object associated with the address \f$a\f$
 * and \f$D(r)\f$ is the empirical *unweighted* distribution over records.
 * 
 * @tparam Ts Types of distributions in the record
 * @param cache the empirical distribution over records
 * @param address the address over which to take the mean
 * @return std::vector<std::variant<Ts...>> 
 */
template<typename O, typename... Ts>
std::vector<std::variant<Ts...>> dist(FilterValueType<O, Ts...>& cache, std::string address) {
    std::vector<std::variant<Ts...>> d;
    for (auto& c : cache->_v) {
        d.push_back(
            std::visit([](const auto& n) -> std::variant<Ts...> { return n.distribution; }, c.map[address])
        );
    }
    return d;
}

/**
 * @brief Computes the score \f$ E_{r \sim D(r)}[\sum_{a: a\ observed} p(r(a))]\f$,
 * where \f$D(r)\f$ is the empirical *unweighted* distribution over records.
 * 
 * @tparam Ts Types of distributions in the record
 * @param cache the empirical distribution over records
 * @return double 
 */
template<typename O, typename... Ts>
double score(FilterValueType<O, Ts...>& cache) {
    double m = 0.0;
    for (auto& c : cache->_v) m += loglikelihood(c);
    return m / cache->_v.size();
}

/**
 * @brief Computes the (approximate) Akaike Information Criterion (AIC) of the model.
 * 
 * \f$AIC = 2 ( \sum_a dim(r(a)) - \sum_{a\ observed} \log p(r(a)) ) \f$, where
 * \f$r\f$ is the maximum likelihood record in the cache.
 * 
 * @tparam Ts Types of distributions in the record
 * @param cache the empirical distribution over records
 * @return double 
 */
template<typename O, typename... Ts>
double aic(FilterValueType<O, Ts...>& cache) {
    double a = 0.0;
    size_t argmax;
    double ll = -std::numeric_limits<double>::infinity();
    for (size_t ix = 0; ix != cache->_v.size(); ix++) {
        auto l = loglikelihood(cache->_v[ix]);
        if (l > ll) {
            argmax = ix;
            ll = l;
        }
    }
    size_t params = 0;
    for (auto& [_, n] : cache->_v[argmax].map) {
        params += std::visit([](auto& n ){ return output_dim<decltype(n.distribution)>::value; }, n);
    }
    return 2 * (params - ll);
}

/**
 * @brief A generic filtering inference algorithm.
 * 
 * Given a probabilistic program \f$p(x,z) = p(x|z)p(z)\f$ and a user-supplied
 * transition kernel, the model is converted into a dynamic filtering model
 * \f$ p(x,z) = p(x_1 | z_1) p(z_1) \prod_{t\geq 2} p(x_t | z_t)p(z_t|x_{t-1}, z_{t-1})\f$
 * using filter_effect. At each timestep, inference is performed using the user-specified
 * inference algorithm to recover an approximate posterior \f$p(z_t | x_t)\f$. That
 * approximate posterior is consumed by the transition callable to generate the new
 * transition dynamics \f$p(z_{t+1}|x_t, z_t)\f$ used in the next filtering step. 
 * 
 * @tparam A The class template of the inference algorithm to use in constructing the approximate posterior
 * @tparam I The input type of the probabilistic program
 * @tparam O The output type of the probabilistic program
 * @tparam Ts The types of the distributions in the probabilistic program
 */
template<
    template<
        typename,
        typename,
        typename,
        template<class, class, class...> class,
        typename...
    > class A,
    class TransKernel,
    typename I,
    typename O,
    typename... Ts
>
struct Filter : protected Inference<A, I, O, FilterValueType<O, Ts...>, WeightedRecord, Ts...> {

    bool has_been_called;
    transition<TransKernel, O, Ts...>& trans;
    std::minstd_rand rng;

    /**
     * @brief Construct a new Filter object
     * 
     * @param f The probabilistic program used to filter.
     * @param queryer A WeightedRecord queryer
     * @param trans user-specified transition dynamics, subclassing transition
     * @param opts inference options
     * @param seed seed for the internal PRNG
     */
    Filter(
        pp_t<I, O, Ts...> f,
        FilterQueryerType<O, Ts...>& queryer,
        transition<TransKernel, O, Ts...>& trans,
        inf_options_t opts,
        size_t seed = 2022
    ) : has_been_called(false), rng(seed), trans(trans), 
        Inference<A, I, O, FilterValueType<O, Ts...>, WeightedRecord, Ts...>(
            filter_effect(f, trans, has_been_called, rng), queryer, opts
        ) {}

    void update_internal_state(FilterValueType<O, Ts...>& posterior) {
        trans.update(posterior);
        this->queryer.clear();
        if (!has_been_called) has_been_called = true;
        this->opts._num_complete = 0;
    }

    /**
     * @brief Executes one filtering step.
     * 
     * This implementation exists only when using an inference algorithm that requires a proposal
     * distribution.
     * 
     * @tparam P Type of the proposal distribution used
     * @param input The input to the probabilistic program at this filtering step
     * @param proposal The proposal struct used in this filtering step, should subclass proposal
     * @return FilterValueType<O, Ts...>
     */
    template<
        class P, 
        template<
            typename,
            typename,
            typename,
            template<class, class, class...> class,
            typename...
        > class G = A
    >
    std::enable_if_t<has_proposal<G>::value == kernel::full, FilterValueType<O, Ts...>> 
    step(I& input, P& proposal) {
        auto return_value = this->operator()(input, proposal);
        update_internal_state(return_value);
        return return_value;
    }

    /**
     * @brief Executes one filtering step. 
     * 
     * This implementation exists only when using an inference algorithm that does not require
     * a proposal distribution
     * 
     * @param input The input to the probabilistic program at this filtering step
     * @return FilterValueType<O, Ts...>
     */
    template<
        template<
            typename,
            typename,
            typename,
            template<class, class, class...> class,
            typename...
        > class G = A
    >
    std::enable_if_t<has_proposal<G>::value == kernel::none, FilterValueType<O, Ts...>> 
    step(I& input) {
        auto return_value = this->operator()(input);
        update_internal_state(return_value);
        return return_value;
    }

};

/**
 * @brief Creates a filtering algorithm.
 * 
 * @tparam A Class template of the inference algorithm used to construct the approximate posterior
 * @tparam I Input type to the probabilistic program
 * @tparam O Output type of the probabilistic program
 * @tparam Ts Types of distributions in the probabilistic program
 * @param f the probabilistic program
 * @param queryer a WeightedRecord queryer instance
 * @param trans user-defined transition dynamics, must subclass transition
 * @param opts inference options
 * @return Filter<A, I, O, Ts...> 
 */
template<
    template<
        typename,
        typename,
        typename,
        template<class, class, class...> class,
        typename...
    > class A,
    class TransKernel,
    typename I,
    typename O,
    typename... Ts
>
Filter<A, TransKernel, I, O, Ts...>
filter(
    pp_t<I, O, Ts...> f, 
    FilterQueryerType<O, Ts...>& queryer, 
    transition<TransKernel, O, Ts...>& trans, 
    inf_options_t opts
) {
    return Filter<A, TransKernel, I, O, Ts...>(f, queryer, trans, opts);
}

#endif // FILTER_H