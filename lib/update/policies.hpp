/**
 * \file
 * This file is part of lppl.
 * Copyright David Rushing Dewhurst, 2023 - present.
 * Released under MIT license
 */

#include <distributions.hpp>
#include <domains.hpp>
#include <update.hpp>

#ifndef UPDATE_POLICIES_H
#define UPDATE_POLICIES_H

/// \todo -- figure out open vs closed universe and representations of their semantics

/**
 * @brief identity function between sets of distributions
 * 
 * @tparam D Input distribution type.
 */
template<typename D>
struct DefaultPolicy {
    using type = D;
};

/**
 * @brief Every continuous distribution type is approximated by a normal distribution.
 * 
 * @tparam D Input distribution type.
 */
template<typename D>
struct NormalPolicy {
    using codom = output_domain<D>::value;
    using underlying_type = codom::type;
    using type = std::conditional<std::is_same_v<underlying_type, double>, Normal, D>::type;
};



#endif  // UPDATE_POLICIES_H
