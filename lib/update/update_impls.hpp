/**
 * \file
 * This file is part of lppl.
 * Copyright David Rushing Dewhurst, 2023 - present.
 * Released under MIT license
 */

#include <distributions.hpp>
#include <inference/filter.hpp>

#include <update.hpp>
#include <update/policies.hpp>

#ifndef UPDATE_IMPLS_H
#define UPDATE_IMPLS_H

constexpr double min(double a, double b) { return (a > b) ? b : a; }

template<typename D, typename MapTo, typename O, typename... Ts>
struct parameter_match;

/**
 * @brief Identity function on parameter value. 
 * 
 * Currently works only with static structure.
 * 
 */
template<
  template<typename> class Constraint, 
  typename BaseType, 
  typename O, 
  typename... Ts
>
struct parameter_match<Parameter<Constraint<BaseType>>, Parameter<Constraint<BaseType>>, O, Ts...> {
  Parameter<Constraint<BaseType>> operator()(FilterValueType<O, Ts...>& result, std::string address) {
    /// \note currently only works with static structure!!!
    auto n = std::get<node_t<Parameter<Constraint<BaseType>>>>(result->_v[0].map[address]);
    return n.distribution;
  }
};

/**
 * @brief Computes normal distribution parameter updates from posterior samples
 * via moment matching.
 * 
 * @tparam O 
 * @tparam Ts 
 */
template<typename D, typename O, typename... Ts> 
struct parameter_match<D, Normal, O, Ts...> {

  using transform_type = mapping<
    typename output_domain<D>::value,
    typename output_domain<Normal>::value
  >;

  double std_max;

  parameter_match(double std_max = 100.0) : std_max(std_max) {}

  Normal operator()(FilterValueType<O, Ts...>& result, std::string address) {
    return Normal(
        mean(
          result, 
          [](double&& x){ return transform_type::forward(x); }, 
        address
      ), 
        min(
          stddev(
            result, 
            [](double&& x){ return transform_type::forward(x); },
            address
          ), 
        std_max
      )
    );
  }
};

/**
 * @brief Computes gamma distribution parameter updates from posterior samples. 
 * 
 * Algorithm from 
 * Ye, Zhi-Sheng, and Nan Chen. "Closed-form estimators for the gamma
 * distribution derived from likelihood equations." The American Statistician 
 * 71.2 (2017): 177-181.
 * 
 * @tparam O 
 * @tparam Ts 
 */
template<typename D, typename O, typename... Ts> 
struct parameter_match<D, Gamma, O, Ts...> {

  using transform_type = mapping<
    typename output_domain<D>::value,
    typename output_domain<Gamma>::value
  >;

  Gamma operator()(FilterValueType<O, Ts...>& result, std::string address) {
    // theta_hat = mean(x log x) - mean(x) * mean( log x )
    // k_hat = mean(x) / theta_hat
    // yes, this could be computed in a single pass, but that's really annoying to write
    /// \todo -- if this is a speed bottleneck, rewrite as single pass computation
    double mean_xlogx = mean(
      result, 
      [](double&& x){ 
        return transform_type::forward(x) * std::log(transform_type::forward(x)); 
      }, 
      address
    );
    double mean_x = mean(result, address);
    double mean_logx = mean(
      result, 
      [](double&& x){ 
        return std::log(transform_type::forward(x)); 
      }, 
      address
    );
    double theta_hat = mean_xlogx - mean_x * mean_logx;
    double k_hat = mean_x / theta_hat;
    return Gamma(k_hat, theta_hat);
  }
};

template <
    template<typename> class Policy,
    typename QueryResult,
    typename I,
    typename O,
    typename... Ts
>
struct ParameterMatching;

/**
 * @brief Computes a variational appxoimation to posterior from posterior samples.
 * 
 * Given samples from the posterior (e.g., as computed by importance sampling or 
 * MCMC methods), `parameter_matching` computes a factorized functional approximation
 * to the posterior suitable for use in message passing or as a new prior distribution
 * for filtering applications. 
 * 
 * `parameter_matching` assumes that the posterior factors as
 * \f$p(z_1,...|x) = \prod_{n \geq 1} p_{\psi_n'}(z_n)\f$,
 * where each component of the density has the same functional form as its counterpart
 * in the prior. The algorithms to compute the optimal parameters
 * \f$\psi_n'\f$ are left up to the implementation.
 * 
 * @tparam Policy 
 * @tparam I 
 * @tparam O 
 * @tparam Ts 
 */
template<
  template<typename> class Policy,
  typename I,
  typename O,
  typename... Ts
>
struct ParameterMatching<Policy, FilterValueType<O, Ts...>, I, O, Ts...>
    : Update<ParameterMatching, Policy, FilterValueType<O, Ts...>, I, O, Ts...> {

    using typed_map_ = typed_map<Policy, I, Ts...>;

    typed_map_ operator()(FilterValueType<O, Ts...>& result) {
      typed_map_ new_map;
      for (auto& address : result->all_addresses()) {
        // we need to do this only once for each of the addresses
        // we cannot assume that each record has all addresses
        // so we must iterate across samples until we have enumerated all addresses
        for (auto& r: result->_v) {
          // if the record contains the address, do it and break
          if (r.map.contains(address)) {
            auto doit = [&address, &new_map, &result](auto& n) {
              if constexpr (!std::is_same_v<I, decltype(n.distribution)>) {
                  // injection into a subspace of distributions allowable by the policy
                  auto new_dist = parameter_match<
                    decltype(n.distribution), 
                    typename Policy<decltype(n.distribution)>::type, 
                    O, 
                    Ts...
                  >()(result, address);
                  new_map.insert_or_assign(address, new_dist);
              }
            };
            std::visit(doit, r.map[address]);
            break;
          }
        }
      }
      return new_map;
    }

};

#endif  // UPDATE_IMPLS_H