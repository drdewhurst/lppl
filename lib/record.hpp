/**  
 * @file
 * This file is part of lppl.
 * Copyright David Rushing Dewhurst, 2022 - present.
 * Released under MIT license
 */

#ifndef RECORD_H
#define RECORD_H

#include <cstddef>
#include <forward_list>
#include <functional>
#include <memory>
#include <set>
#include <sstream>
#include <string>
#include <unordered_map>
#include <type_traits>
#include <utility>
#include <variant>

#include <domains.hpp>
#include <functional_util.hpp>
#include <distributions.hpp>


const auto __COMPILERNG__ = [](){ return 2022ULL; };

using namespace Distributions;

/**
 * @brief Node-level interpretations.
 * 
 * + interp_t::standard: no special interpretation
 * + interp_t::replay: the node should be replayed through the record, pretending as
 *  though the value in the node had been sampled from the distribution, with
 *  no other changes to node structure other than updated log probability
 * + interp_t::replace: the distribution in the calling function should be replaced by the distribution
 *  stored in the record at this address
 * + interp_t::condition: the node should be conditioned on the value in the node (and hence
 *  the node is converted into an observed node)
 * + interp_t::block: converts the node to untraced randomness
 * + interp_t::propose: the node has been proposed
 * + interp_t::parameter: this node is a point parameter value
 * 
 */
enum class interp_t {
    standard,
    replay,
    replace,
    condition,
    block,
    propose,
    parameter
};

/**
 * @brief A fundamental data structure that includes address, distribution,
 * sampled value type, score, whether the value was observed, and a markov process
 * over interpretations
 * 
 * @tparam D the type of the distribution in the node; the value type is deduced
 *  from the distribution
 */
template<typename D>
struct node_t {
    std::string address;
    D distribution;
    decltype(std::declval<D>().sample(__COMPILERNG__)) value;
    double logprob;
    bool obs;
    interp_t interp;
    interp_t last_interp;
    
    /**
     * @brief Returns a string representation of the node, not including interpretation.
     * 
     * @return std::string 
     */
    std::string string() const {
        std::string r = "";
        r += "node_t(address=" + address;
        r += ", distribution=" + distribution.string();
        r += ", value=" + stringify(value);
        r += ", logprob=" + std::to_string(logprob);
        r += obs ? ", obs=true" : ", obs=false";
        r += ")";
        return r;
    }
};

/**
 * @brief Creates a node with specified address by sampling value from corresponding
 *  distribution, then scoring it.
 * 
 */
template<typename D, typename RNG>
node_t<D> sample(std::string address, D dist, RNG& rng) {
    auto value = dist.sample(rng);
    return node_t<D> {
        address,
        dist,
        value,
        dist.logprob(value),
        false,
        interp_t::standard,
        interp_t::standard
    };
}

/**
 * @brief Creates a node with specified address by scoring passed value from
 *  corresponding distribution.
 * 
 */
template<typename D, typename V>
node_t<D> observe(std::string address, D dist, V value) {
    return node_t<D> {
        address,
        dist,
        value,
        dist.logprob(value),
        true,
        interp_t::condition,
        interp_t::condition
    };
}

/**
 * @brief The return type of calling sample on a distribution object.
 * 
 * There is an injection from the space of distributions into the space of value types; the space
 * of value types is computed at compile time and is not needed as a template argument to sample calls.
 * 
 * @tparam D The type of distribution for which to compute the sample value type
 */
template<typename D>
using DSType = decltype(std::declval<D>().sample(__COMPILERNG__));

template<typename... Ts>
using var_ret_t = std::variant<DSType<Ts>...>;

template<typename... Ts>
using var_node_t = std::variant<node_t<Ts>...>;

template<typename... Ts>
using node_map_t = std::unordered_map<std::string, var_node_t<Ts...>>;

/**
 * @brief Record-level intepretation. Setting a record-level interpretation apart
 *  from record_interp_t::standard will force calls to sample/oberve to disregard any node-level
 *  interpretation.
 * 
 * \todo refactor as enum class
 * 
 * + record_interp_t::standard: no special interpretation
 * + record_interp_t::replay: replay the record through the program
 * + record_interp_t::replace: replace distribution in each sample call with the one in the record
 * + record_interp_t::rewrite: replace followed by replay
 * + record_interp_t::block_obs: block all observed sites
 * + record_interp_t::block_sample: block all sample sites
 * 
 */
enum class record_interp_t {
    standard,
    replay,
    replace,
    rewrite,
    block_obs,
    block_sample
};

/**
 * @brief A fundamental data structure that holds a mapping from addresses to
 *  nodes, an insertion order, and a record-level interpretation.
 * 
 * @tparam Ts types of all nodes contained in the record. 
 */
template<typename... Ts>
struct record_t {
    node_map_t<Ts...> map;
    std::vector<std::string> addresses;
    std::forward_list<record_interp_t> interp;

    /**
     * @brief Clears the underlying map and insertion order.
     * 
     */
    void clear() {
        map.clear();
        addresses.clear();
    }

    /**
     * @brief Insert the node into the record at the specified address
     * 
     */
    void insert(std::string address, std::variant<node_t<Ts>...>& node) {
        map.insert_or_assign(address, node);
        addresses.push_back(address);
    }

    /**
     * @brief Copies data from each unobserved sample node from the passed record into this record
     * 
     * @param r the record from which to copy the data
     */
    void copy_unobserved_from_other(record_t<Ts...>& r) {
        auto is_obs = [](auto& n) -> bool { return n.obs; };
        for (auto& address : r.addresses) {
            if (!std::visit(is_obs, r.map[address])) {
                if (!map.contains(address)) {
                    insert(address, r.map[address]);
                } else {
                    map.insert_or_assign(address, r.map[address]);
                }
            }
        }
        interp = r.interp;
    }

    /**
     * @brief Copies data from all nodes from the passed record into this record
     * 
     * @param r the record from which to copy the data
     */
    void copy_from_other(record_t<Ts...>& r) {
        for (auto& address : r.addresses) {
            if (!map.contains(address)) {
                insert(address, r.map[address]);
            } else {
                map.insert_or_assign(address, r.map[address]);
            }
        }
        interp = r.interp;
    }

};

/**
 * @brief Samples a value into a node stored at the address by drawing a value from
 *  the specified distribution. Returns the underlying value of the created node.
 * 
 * @tparam D The type of distribution from which to sample
 * @tparam RNG The type of PRNG to use
 * @tparam Ts The distribution types contained in the passed record
 * @param r the record into which to sample
 * @param address the address at which to sample
 * @param dist the distribution from which to draw sample(s)
 * @param rng the PRNG to use for sampling
 * @return DSType<D> the sampled value
 */
template<typename D, typename RNG, typename... Ts>
DSType<D> sample(record_t<Ts...>& r, std::string address, D dist, RNG& rng) {
    if ((r.interp.empty()) || (r.interp.front() == record_interp_t::standard)) {
        return _sample_with_node_interp<D, RNG, Ts...>(r, address, dist, rng);
    } else {
        return _sample_with_record_interp<D, RNG, Ts...>(r, address, dist, rng);
    }
}

template<typename D, typename RNG, typename... Ts>
DSType<D> _sample_with_record_interp(record_t<Ts...>& r, std::string address, D dist, RNG& rng) {
    if (r.interp.front() == record_interp_t::replay) {
        return _replay<D, RNG, Ts...>(r, address, dist, rng);
    } else if (r.interp.front() == record_interp_t::block_sample) {
        return _block_sample<D, RNG, Ts...>(r, address, dist, rng);
    } else if (r.interp.front() == record_interp_t::replace) {
        return _replace<D, RNG, Ts...>(r, address, dist, rng);
    } else if (r.interp.front() == record_interp_t::rewrite) {
        return _rewrite<D, RNG, Ts...>(r, address, dist, rng);
    // no need for record_interp_t::block_obs here as sampling only
    // TODO is that true? What if the site is conditioned?
    } else {
        return _sample_with_node_interp<D, RNG, Ts...>(r, address, dist, rng);
    }
}

template<typename... Ds>
using data_types = unique_variant_t<var_ret_t<Ds...>>;

template<typename... Ds>
using param_store = std::unordered_map<std::string, data_types<Ds...>>;

template<typename D, typename RNG, typename... Ts>
DSType<D> _sample_with_node_interp(record_t<Ts...>& r, std::string address, D dist, RNG& rng) {
    if (!r.map.contains(address)) {
        return _standard(r, address, dist, rng);
    } else {
        auto get = [&](auto& node) -> data_types<Ts...> {
            if (node.interp == interp_t::replay) return _replay(r, address, dist, rng);
            else if (node.interp == interp_t::replace) return _replace(r, address, dist, rng);
            else if (node.interp == interp_t::condition) return _condition(r, address, dist);
            else if (node.interp == interp_t::block) return _block_sample(r, address, dist, rng);
            else return _standard(r, address, dist, rng);
        };
        return std::get<DSType<D>>(std::visit(get, r.map[address]));
    }
}

template<typename D, typename RNG, typename... Ts>
DSType<D> _block_sample(record_t<Ts...>& r, std::string address, D dist, RNG& rng) {
    r.map.erase(address);
    return dist.sample(rng);
}

template<typename D, typename RNG, typename... Ts>
DSType<D> _standard(record_t<Ts...>&r, std::string address, D dist, RNG& rng) {
    auto node = sample(address, dist, rng);
    if (!r.map.contains(address)) r.addresses.push_back(address);
    r.map.insert_or_assign(address, node);
    return node.value;
}

template<typename D, typename RNG, typename... Ts>
DSType<D> _replay(record_t<Ts...>& r, std::string address, D dist, RNG& rng) {
    if (!r.map.contains(address)) return _standard<D, RNG, Ts...>(r, address, dist, rng);
    auto value = std::get<DSType<D>>(
        std::visit(
            [](auto& node) -> data_types<Ts...> { return node.value; },
            r.map[address]
        )
    );
    auto last_interp = std::visit(
        [](auto& node) -> interp_t { return node.last_interp; },
        r.map[address]
    );
    auto obs = std::visit(
        [](auto& node) -> bool { return node.obs; },
        r.map[address]
    );
    // note orig node can be originally only partially instantiated
    // *does not require* origin node to have dist
    auto node = node_t<D> { 
        .address=address,
        .distribution=dist,
        .value=value,
        .logprob=dist.logprob(value),
        .obs=obs,
        .interp=last_interp,
        .last_interp=interp_t::replay
    };
    r.map.insert_or_assign(address, node);
    return node.value;
}

template<typename D, typename RNG, typename... Ts>
DSType<D> _replace(record_t<Ts...>& r, std::string address, D orig_dist, RNG& rng) {
    if (!r.map.contains(address)) return _standard<D, RNG, Ts...>(r, address, orig_dist, rng);
    D dist = std::get<D>(
        std::visit(
            [](auto& node) -> std::variant<Ts...> { return node.distribution; },
            r.map[address]
        )
    );
    return _standard<D, RNG, Ts...>(r, address, dist, rng);
}

template<typename D, typename RNG, typename... Ts>
DSType<D> _rewrite(record_t<Ts...>& r, std::string address, D orig_dist, RNG& rng) {
    if (!r.map.contains(address)) return _replay<D, RNG, Ts...>(r, address, orig_dist, rng);
    D dist = std::get<D>(
        std::visit(
            [](auto& node) -> std::variant<Ts...> { return node.distribution; },
            r.map[address]
        )
    );
    return _replay<D, RNG, Ts...>(r, address, dist, rng);
}

template<typename D, typename... Ts> 
DSType<D> _condition(record_t<Ts...>& r, std::string address, D dist) {
    auto value = std::get<DSType<D>>(
        std::visit(
            [](auto& node) -> data_types<Ts...> { return node.value; },
            r.map[address]
        )
    );
    auto last_interp = std::visit(
        [](auto& node) -> interp_t { return node.last_interp; },
        r.map[address]
    );
    auto node = node_t<D> { 
        .address=address,
        .distribution=dist,
        .value=value,
        .logprob=dist.logprob(value),
        .obs=true,
        .interp=interp_t::condition,
        .last_interp=last_interp
    };
    r.map.insert_or_assign(address, node);
    return node.value;
}

/**
 * @brief Scores the passed value against the distribution, storing the result in the
 *  record at the specified address.
 * 
 * @tparam D The type of distribution from which to sample
 * @tparam V The value type
 * @tparam Ts The distribution types contained in the passed record
 * @param r the record into which to store the scored value
 * @param address the address at which to store the scored value
 * @param dist the distribution against which to score values
 * @param v the value to be scored against the distribution
 * @return V the scored value
 */
template<typename D, typename V, typename... Ts>
V observe(record_t<Ts...>& record, std::string address, D dist, V value) {
    if (record.interp.empty()) {
        return _observe_with_node_interp<D, V, Ts...>(record, address, dist, value);
    } else if (record.interp.front() == record_interp_t::standard) {
        return observe<D, V, Ts...>(record, address, dist, value);
    } else {
        return _observe_with_record_interp<D, V, Ts...>(record, address, dist, value);
    }
}

template<typename V, typename... Ts>
struct ParamConstructor {

    record_t<Ts...>& r;
    std::string address;

    using type = typename V::type;

    ParamConstructor(record_t<Ts...>& r, std::string address)
        : r(r), address(address) {}

    type operator()(type value) {
        auto node = node_t<Parameter<V>> {
            address,
            Parameter<V>(value),
            value,
            0.0,
            false,
            interp_t::parameter,
            interp_t::parameter
        };
        if (!r.map.contains(address)) r.addresses.push_back(address);
        r.map.insert_or_assign(address, node);
        return value;
    }

};

template<typename V, typename... Ts>
ParamConstructor<V, Ts...> param(record_t<Ts...>& r, std::string address) {
    return ParamConstructor<V, Ts...>(r, address);
}

template<typename D, typename V, typename... Ts>
V _observe_with_record_interp(record_t<Ts...>& record, std::string address, D dist, V value) {
    // no need for record_interp_t::replay since no differentiated node effect
    // no need for record_interp_t::block_sample since observed
    if (record.interp.front() == record_interp_t::block_obs) {
        return _block_observe<D, V, Ts...>(record, address, dist, value);
    } else if (record.interp.front() == record_interp_t::replace) {
        return _replace_observe<D, V, Ts...>(record, address, dist, value);
    } else {
        return _observe_with_node_interp<D, V, Ts...>(record, address, dist, value);
    }
}

template<typename D, typename V, typename... Ts>
V _replace_observe(record_t<Ts...>& r, std::string address, D orig_dist, V value) {
    if (!r.map.contains(address)) return _observe<D, V, Ts...>(r, address, orig_dist, value);
    D dist = std::get<D>(
        std::visit(
            [](auto& node) -> std::variant<Ts...> { return node.distribution; },
            r.map[address]
        )
    );
    return _observe(r, address, dist, value);
}

template<typename D, typename V, typename... Ts>
V _observe_with_node_interp(record_t<Ts...>& record, std::string address, D dist, V value) {
    if (!record.map.contains(address)) {
        return _observe(record, address, dist, value);
    } else {
        using ThisT = decltype(std::declval<D>().sample(__COMPILERNG__));
        auto get = [&](auto& node) -> data_types<Ts...> {
            // observe *means* condition, and replay doesn't make sense here
            if (node.interp == interp_t::block) {
                return _block_observe(record, address, dist, value);
            } else if (node.interp == interp_t::replace) {
                return _replace_observe(record, address, dist, value);
            } else {
                return _observe(record, address, dist, value);
            }
        };
        return std::get<ThisT>(
            std::visit(get, record.map[address])
        );
    }
}

template<typename D, typename V, typename... Ts>
V _observe(record_t<Ts...>& record, std::string address, D dist, V value) {
    if (!record.map.contains(address)) record.addresses.push_back(address);
    record.map.insert_or_assign(address, observe(address, dist, value));
    return value;
}

template<typename D, typename V, typename... Ts>
V _block_observe(record_t<Ts...>& r, std::string address, D, V value) {
    r.map.erase(address);
    return value;
}

/**
 * @brief Returns a string representation of the record. Does not display
 *  node- or record-level interpretations.
 * 
 * \todo reimplement as instance of generic fold
 * 
 */
template <typename... Ts>
std::string display(record_t<Ts...>& record) {
    auto disp = [](auto& node) -> std::string { return node.string(); };
    std::string s = "record_t(\n";
    for (const auto& address : record.addresses) {
        s += "  address=" + address + ", node=" + std::visit(disp, record.map[address]) + "\n";
    }
    s += ")";
    return s;
}

/**
 * @brief Computes the cumulative log probability of the record, \f$ \log p(x, z) \f$.
 * 
 */
template<typename... Ts>
double logprob(record_t<Ts...>& record) {
    auto get_lp = [](auto node) -> double { return node.logprob; };
    double lp = 0.0;
    for (const auto& [_, v] : record.map) {
        lp += std::visit(get_lp, v);
    }
    return lp;
}

/**
 * @brief Computes the scaled cumulative log-likelihood of the record, \f$ \sum_{a\ observed} \beta(\log p(r(a))) \f$. 
 * 
 * @tparam Ts The types of the distributions in the program
 * @param record the record for which to compute the log likelihood
 * @param scale \f$ \beta:\ double \rightarrow double \f$ scales the likelihood values
 * @return double 
 */
template<typename... Ts>
double loglikelihood(record_t<Ts...>& record, std::function<double(double)>&& scale) {
    auto get_ll = [](auto node) -> double { return node.obs ? node.logprob : 0.0; };
    double ll = 0.0;
    for (const auto& [_, v] : record.map) {
        ll += scale(std::visit(get_ll, v));
    }
    return ll;
}

/**
 * @brief Computes the scaled cumulative log-likelihood of the record on a per-address basis, \f$ \sum_{a\ observed} \beta(a, \log p(r(a))) \f$. 
 * 
 * @tparam Ts The types of the distributions in the program
 * @param record the record for which to compute the log likelihood
 * @param scale \f$ \beta:\ string \rightarrow double \rightarrow double \f$ scales the likelihood values
 * @return double 
 */
template<typename... Ts>
double loglikelihood(record_t<Ts...>& record, std::function<double(std::string&, double)>&& scale) {
    auto get_ll = [](auto node) -> double { return node.obs ? node.logprob : 0.0; };
    double ll = 0.0;
    for (const auto& [k, v] : record.map) {
        ll += scale(k, std::visit(get_ll, v));
    }
    return ll;
}

/**
 * @brief Computes the cumulative log-likelihood of the record, \f$ \sum_{a\ observed} \log p(r(a)) \f$. 
 * 
 * @tparam Ts The types of the distributions in the program
 * @param record the record for which to compute the log likelihood
 * @return double 
 */
template<typename... Ts>
double loglikelihood(record_t<Ts...>& record) {
    return loglikelihood(record, [](double x){ return x; });
}

/**
 * @brief Computes the cumulative log-latent of the record, \f$ \sum_{a\ unobserved} \log p(r(a)) \f$. 
 * 
 * @tparam Ts The types of the distributions in the program
 * @param record the record for which to compute the log latent
 * @return double 
 */
template<typename... Ts>
double loglatent(record_t<Ts...>& record) {
    auto get_ll = [](auto node) -> double { return node.obs ? 0.0 : node.logprob; };
    double ll = 0.0;
    for (const auto& [_, v] : record.map) {
        ll += std::visit(get_ll, v);
    }
    return ll;
}

/**
 * @brief A shorthand for probabilistic program type. Probabilistic programs are
 *  callables that take a single input of type I, output a single value of type O, 
 *  and contain an arbitrary positive number of distributions of type Ts...
 * 
 * To be well-formed, a probabilistic program should terminate with probability one.
 * 
 */
template<typename I, typename O, typename... Ts>
using pp_t = std::function<O(record_t<Ts...>&, I)>;

#endif  // RECORD_H
