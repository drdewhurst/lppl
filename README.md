# `lppl`

`lppl` is a low-resource universal probabilistic programming framework.

Homepage: https://davidrushingdewhurst.com/lppl/ 

## Build
`cd build && cmake .. && make`. Requires (a) a compiler that supports C++ 20 and (b) CMake v3.20.0 or later. 

**Please note** that `lppl` has been tested on Apple clang v12.0.0 on OSX, Mingw-w64 on Windows 10, and g++ 11.3.0 on ubuntu. It *should* compile under any standards compliant C++20 compiler, but that is not guaranteed. Please submit build errors here: https://gitlab.com/drdewhurst/lppl/-/issues.  

## Test
Run any of the tests that show up in the `build` directory after you build. E.g., `./record_test`. **Please note** that deterministic results are guaranteed under specific hardware configurations, but not across operating systems.

## Documentation
Here: https://davidrushingdewhurst.com/lppl/docs/index.html  

## Examples
Here: https://davidrushingdewhurst.com/lppl/examples/

## License etc.

`lppl` is licensed under the MIT license. Enjoy! Copyright David Rushing Dewhurst, 2022 - present.
