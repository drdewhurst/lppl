/* 
 * This file is part of lppl.
 * Copyright David Rushing Dewhurst, 2022 - present.
 * Released under MIT license
 */

#include <cassert>
#include <chrono>
#include <cmath>
#include <functional>
#include <iostream>
#include <limits>
#include <optional>
#include <random>
#include <string>
#include <variant>
#include <vector>

#include <distributions.hpp>
#include <effects.hpp>
#include <inference/inference.hpp>
#include <inference/metropolis/base.hpp>
#include <plate.hpp>
#include <record.hpp>


std::minstd_rand file_rng(202);


struct my_state {
    std::minstd_rand rng;
    double data;

    my_state(double data)
        : rng(2022), data(data) {}

};


double my_pp(record_t<Normal, Gamma>& r, std::shared_ptr<my_state> state) {
    auto loc = sample(r, "loc", Normal(0.0, 3.0), state->rng);
    auto scale = sample(r, "scale", Gamma(), state->rng);
    auto obs = observe(r, "obs", Normal(loc, scale), state->data);
    return std::pow(obs, 2.0);
}

int test_ancestor() {

    auto s = std::make_shared<my_state>(-3.0);
    pp_t<std::shared_ptr<my_state>, double, Normal, Gamma> f = my_pp;
    auto q = weighted_mean<double, Normal, Gamma>("loc");

    size_t thin = 25;
    size_t burn = 500;
    size_t nsamp = 25;
    auto opts = inf_options_t(thin, burn, burn + thin * nsamp);
    auto infer1 = inference<AncestorMetropolis>(f, *q, opts);
    auto start_time = std::chrono::high_resolution_clock::now();
    auto post_mean = infer1(s);
    auto elapsed = std::chrono::high_resolution_clock::now() - start_time;
    auto millis = std::chrono::duration_cast<std::chrono::milliseconds>(elapsed).count();
    
    std::cout << "Posterior mean = " << post_mean << std::endl;
    std::cout << "computed posterior marginal mean in " << millis << " milliseconds" << std::endl;

    auto record_q = weighted_record<double, Normal, Gamma>();
    auto infer2 = inference<AncestorMetropolis>(f, *record_q, opts);
    start_time = std::chrono::high_resolution_clock::now();
    auto some_records = infer2(s);
    elapsed = std::chrono::high_resolution_clock::now() - start_time;
    millis = std::chrono::duration_cast<std::chrono::milliseconds>(elapsed).count();
    std::cout << "computed posterior over records in " << millis << " milliseconds" << std::endl;

    auto r_samp = some_records->sample(s->rng);
    std::cout << "Sampled record:\n" << display(*r_samp) << std::endl;

    return 0;
}

struct my_kernel {
    enum class which_one {loc, scale};
    which_one last = which_one::loc;

    // record_t<Normal, Gamma> 
    // operator()(record_t<Normal, Gamma>& r) {
    double operator()(record_t<Normal, Gamma>& new_r, record_t<Normal, Gamma> r) {
        if (last == which_one::loc) {
            double last_scale = std::visit([](auto& n){ return n.value; }, r.map["scale"]);
            // propose a log scale value and then exp
            auto prop = Normal(last_scale, 0.25);
            auto value = prop.sample(file_rng);
            transfer(
                new_r,
                    (node_t<Gamma>) {
                        .address = "scale",
                        .value = std::exp(value),
                        .logprob = prop.logprob(value),
                        .obs = false,
                        .interp = interp_t::propose
                    }
            );
            last = which_one::scale;
        } else {
            double last_loc = std::visit([](auto& n){ return n.value; }, r.map["loc"]);
            auto prop = Normal(last_loc, 0.25);
            auto value = prop.sample(file_rng);
            transfer(
                new_r,
                (node_t<Normal>) {
                    .address = "loc",
                    .value = value,
                    .logprob = prop.logprob(value),
                    .obs = false,
                    .interp = interp_t::propose
                }
            );
            last = which_one::loc;
        }
        return loglatent(new_r);
    }
};

int test_generic() {

    my_kernel proposal;
    pp_t<record_t<Normal, Gamma>, double, Normal, Gamma> k = proposal;
    auto s = std::make_shared<my_state>(3.0);
    pp_t<std::shared_ptr<my_state>, double, Normal, Gamma> f = my_pp;
    // auto q = QueryerCollection<double, double, WeightedMean, Normal, Gamma>({"loc", "scale"});
    auto q = weighted_mean<double, Normal, Gamma>("scale");

    size_t thin = 100;
    size_t burn = 10000;
    size_t nsamp = 250;
    auto opts = inf_options_t(thin, burn, burn + thin * nsamp);
    auto infer1 = inference<GenericMetropolis>(f, *q, opts);
    auto start_time = std::chrono::high_resolution_clock::now();
    auto post_mean_scale = infer1(s, k);
    auto elapsed = std::chrono::high_resolution_clock::now() - start_time;
    auto millis = std::chrono::duration_cast<std::chrono::milliseconds>(elapsed).count();
    
    std::cout << post_mean_scale << std::endl;
    std::cout << "computed posterior marginal mean scale in " << millis << " milliseconds" << std::endl;

    return 0;
}

template<size_t N>
double var_dice_model(record_t<Normal, static_plate<Categorical, N>>& r, double obs_value) {
    auto dice_values = sample(r, "dice_values", static_plate<Categorical, N>(6), file_rng);
    double value = 0.0;
    for (size_t ix = 0; ix != N; ix++) {
        value += sample(r, "payoff " + std::to_string(ix), Normal(0.0, 1 + dice_values->at(ix)), file_rng);
    }
    return observe(r, "value", Normal(value, 1.0), obs_value);
}

int test_plate_ancestor() {
    constexpr size_t dim = 4;
    auto q = weighted_mean<double, Normal, static_plate<Categorical, dim>>("payoff 2");
    size_t thin = 25;
    size_t burn = 500;
    size_t nsamp = 25;
    auto opts = inf_options_t(thin, burn, burn + thin * nsamp);

    double obs_value = -3.0;  // bad luck!
    pp_t<double, double, Normal, static_plate<Categorical, dim>> f = var_dice_model<dim>;
    auto infer = inference<AncestorMetropolis>(f, *q, opts);
    auto post_mean = infer(obs_value);
    std::cout << "posterior mean of payoff from dice 2 is " << post_mean << std::endl;

    return 0;
}

int main(int argc, char ** argv) {
    auto status = std::vector<int>({
        test_ancestor(),
        test_generic(),
        test_plate_ancestor()
    });
    if (*std::max_element(status.begin(), status.end()) > 0) {
        std::cout << "~~~ Test failed; read log for more. ~~~\n";
        return 1;
    } else {
        std::cout << "~~~ All tests passed. ~~~\n";
        return 0;
    }
}