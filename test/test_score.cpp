/* 
 * This file is part of lppl.
 * Copyright David Rushing Dewhurst, 2022 - present.
 * Released under MIT license
 */

#include <functional>
#include <iostream>
#include <vector>

#include <distributions.hpp>
#include <query.hpp>
#include <record.hpp>
#include <score.hpp>
#include <inference/inference.hpp>
#include <inference/importance/likelihood.hpp>

using namespace Distributions;

std::minstd_rand rng(2002);

double univar_opt(record_t<Normal, Score<double>>& r, std::function<double(double)> f) {
    auto param = sample(r, "param", Normal(0.0, 4.0), rng);
    return score(r, "result", f, param);
}


int test_score_basic() {
    // maximum at 5.0
    pp_t<std::function<double(double)>, double, Normal, Score<double>> f = univar_opt;
    std::function<double(double)> fn = [](double x) { return -10 * std::pow(x - 5.0, 2); };
    auto q = weighted_mean<double, Normal, Score<double>>("param");
    auto opts = inf_options_t(10000);
    auto infer = inference<LikelihoodWeighting>(f, *q, opts);
    auto res = infer(fn);

    std::cout << "res = " << res << std::endl;

    return 0;
}


int main(int argc, char ** argv) {
    auto status = std::vector<int>({
        test_score_basic()
    });
    if (*std::max_element(status.begin(), status.end()) > 0) {
        std::cout << "~~~ Test failed; read log for more. ~~~\n";
        return 1;
    } else {
        std::cout << "~~~ All tests passed. ~~~\n";
        return 0;
    }
}