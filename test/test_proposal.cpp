/* 
 * This file is part of lppl.
 * Copyright David Rushing Dewhurst, 2022 - present.
 * Released under MIT license
 */

#include <distributions.hpp>
#include <record.hpp>
#include <inference/proposal.hpp>
#include <inference/importance/generic.hpp>
#include <inference/inference.hpp>
#include <plate.hpp>
#include <query.hpp>

#include <iostream>
#include <numeric>
#include <random>
#include <string>
#include <utility>
#include <vector>


using namespace Distributions;

std::minstd_rand rng(202);


std::pair<double, double>
normal_model(record_t<Normal>& r, double obs_val) {
    auto loc = sample(r, "loc", Normal(0.0, 2.0), rng);
    auto log_scale = sample(r, "log_scale", Normal(-1.0, 0.5), rng);
    observe(r, "obs", Normal(loc, std::exp(log_scale)), obs_val);
    return std::make_pair(loc, log_scale);
}

struct my_proposal : proposal<my_proposal, Normal> {
    std::pair<record_t<Normal>, double> generate() {
        record_t<Normal> r;
        sample(r, "loc", Normal(0.0, 10.0), rng);
        sample(r, "log_scale", Normal(-1.0, 2.0), rng);
        return std::make_pair(r, logprob(r));
    }
};

struct my_second_proposal : proposal<my_second_proposal, Normal> {
    std::pair<record_t<Normal>, double> generate() {
        record_t<Normal> r_ret;
        propose<Normal>(r_ret, "loc", Uniform(-15.0, 15.0), rng);
        propose<Normal>(r_ret, "log_scale", Uniform(-3.0, 3.0), rng);
        return std::make_pair(r_ret, logprob(r_ret));
    }
};

int test_proposal_incremental() {

    std::cout << "~~~ using normal proposal for normal model ~~~" << std::endl;
    my_proposal prop;
    auto r = prop();
    std::cout << display(r.first) << std::endl;

    double data = 8.0;

    using otype = std::pair<double, double>;

    pp_t<double, otype, Normal> f = normal_model;
    auto opts = inf_options_t(100);

    auto record_q = weighted_record<otype, Normal>();

    auto infer = inference<ImportanceSampling>(f, *record_q, opts);
    auto records = infer(data, prop);
    auto r_samp = records->sample(rng);
    std::cout << "Sampled record:\n" << display(*r_samp) << std::endl;

    for (size_t ix = 0; ix != 3; ++ix) {
        auto param_samp = records->sample_output(rng);
        std::cout << "sampled loc = " << param_samp->first << ", log_scale = " << param_samp->second << std::endl;
    }

    auto mean_q = weighted_mean<otype, Normal>("loc");
    auto infer2 = inference<ImportanceSampling>(f, *mean_q, opts);
    auto mean_loc = infer2(data, prop);
    std::cout << "Mean loc = " << mean_loc << std::endl;

    std::cout << "~~~ using uniform proposal for normal model ~~~" << std::endl;
    my_second_proposal prop2;
    auto r2 = prop2();
    std::cout << "r2 has logprob = " << r2.second << std::endl;

    auto record_q2 = weighted_record<otype, Normal>();
    auto infer3 = inference<ImportanceSampling>(f, *record_q2, opts);
    records = infer3(data, prop2);
    r_samp = records->sample(rng);
    std::cout << "Sampled record:\n" << display(*r_samp) << std::endl;

    auto mean_q2 = weighted_mean<otype, Normal>("loc");
    auto infer4 = inference<ImportanceSampling>(f, *mean_q2, opts);
    mean_loc = infer4(data, prop2);
    std::cout << "Mean loc = " << mean_loc << std::endl;

    auto post_loc_q2 = weighted_value<double, otype, Normal>("loc");
    auto infer5 = inference<ImportanceSampling>(f, *post_loc_q2, opts);
    auto post_loc = infer5(data, prop2);
    auto pts = post_loc->sample(rng, 15);
    std::cout << "~~~ sampled some points from loc posterior ~~~" << std::endl;
    for (auto& pt : *pts) std::cout << pt << std::endl;
    // what's the mean loc if loc is actually greater than 8?
    auto ge_8 = (*post_loc) \
        .filter(
            [](double& v){ return v > 8.0; }
        ) \
        .reduce(
            [](std::vector<double>& v) {return std::accumulate(v.begin(), v.end(), 0.0) / v.size(); }
        );
    std::cout << "`~~~ E[loc | loc > 8, data] = " << ge_8 << std::endl;

    return 0;
}


int main(int argc, char ** argv) {
    auto status = std::vector<int>({
        test_proposal_incremental()
    });
    if (*std::max_element(status.begin(), status.end()) > 0) {
        std::cout << "~~~ Test failed; read log for more. ~~~\n";
        return 1;
    } else {
        std::cout << "~~~ All tests passed. ~~~\n";
        return 0;
    }
}

