/* 
 * This file is part of lppl.
 * Copyright David Rushing Dewhurst, 2022 - present.
 * Released under MIT license
 */

#include <distributions.hpp>
#include <record.hpp>

#include <inference/filter.hpp>
#include <inference/filter/kernels.hpp>
#include <inference/proposal.hpp>
#include <inference/importance/likelihood.hpp>
#include <inference/importance/generic.hpp>
#include <inference/metropolis/base.hpp>

#include <query.hpp>

#include <update.hpp>
#include <update/policies.hpp>
#include <update/update_impls.hpp>

#include <functional>
#include <iostream>
#include <string>
#include <utility>
#include <vector>

using namespace Distributions;

std::minstd_rand rng(202);

double normal_model(record_t<Normal, Gamma>& r, double data) {
    auto loc = sample(r, "loc", Normal(), rng);
    auto scale = sample(r, "scale", Gamma(0.25, 5.0), rng);
    observe(r, "obs", Normal(loc, scale), data);
    return loc;
}

struct normal_trans_kernel 
    : transition<normal_trans_kernel, double, Normal, Gamma> {

    double loc;

    void update(FilterValueType<double, Normal, Gamma>& cache) {
        std::cout << "normal_trans_kernel: loc estimate before update = " << loc << std::endl;
        size_t ix = 0;
        loc = 0.0;
        for (auto& r : cache->_v) {
            loc += std::visit([&cache, &ix](auto& n){ return n.value * cache->prob_at(ix); }, r.map["loc"]);
            ++ix;
        }
        loc = 0.1 * loc + 0.9 * std::visit([](auto& n){ return n.value; }, cache->_v[0].map["obs"]);
    }

    std::pair<record_t<Normal, Gamma>, double> generate() {
        record_t<Normal, Gamma> r;
        sample(r, "loc", Normal(loc, 1.0), rng);
        sample(r, "scale", Gamma(0.5, 0.5), rng);
        return std::make_pair(r, logprob(r));
    }

};

struct normal_trans_kernel_2 
    : transition<normal_trans_kernel_2, double, Normal, Gamma> {

    transition_kernels::rw_trans_kernel<Normal, Normal, Gamma> loc_trans;
    transition_kernels::rw_trans_kernel<Gamma, Normal, Gamma> scale_trans;

    normal_trans_kernel_2() 
        : loc_trans(transition_kernels::rw_trans_kernel<Normal, Normal, Gamma>("loc", 0.0, 1.0)), 
          scale_trans(transition_kernels::rw_trans_kernel<Gamma, Normal, Gamma>("scale", 0.5, 0.5))
          {}

    void update(FilterValueType<double, Normal, Gamma>& cache) {
        loc_trans.update(cache);
        scale_trans.update(cache);
    }

    std::pair<record_t<Normal, Gamma>, double> generate() {
        record_t<Normal, Gamma> r;
        loc_trans.generate(r, rng);
        scale_trans.generate(r, rng);
        return std::make_pair(r, loglatent(r));
    }
};

struct normal_proposal : proposal<normal_proposal, Normal, Gamma> {

    // use the past transition dynamics to inform our proposal! :)
    normal_trans_kernel& trans;

    normal_proposal(normal_trans_kernel& trans) : trans(trans) {}

    std::pair<record_t<Normal, Gamma>, double> generate() {
        record_t<Normal, Gamma> r;
        sample(r, "loc", Normal(trans.loc, 2.0), rng);
        sample(r, "scale", Gamma(2.0, 2.0), rng);
        return std::make_pair(r, logprob(r));
    }
};

int test_filter_basic() {

    double data = 3.0;
    size_t iters = 5;
    pp_t<double, double, Normal, Gamma> f = normal_model;
    auto opts = inf_options_t(100);

    auto get_value = [](auto& n) -> double { return n.value; };
    auto q = weighted_record<double, Normal, Gamma>();
    normal_trans_kernel trans;
    trans.loc = 0;
    auto infer = filter<LikelihoodWeighting, normal_trans_kernel>(f, *q, trans, opts);
    auto mean_ = [](std::vector<double>& v) {return std::accumulate(v.begin(), v.end(), 0.0) / v.size(); };

    // filter 1!
    std::cout << "~~~ using default particle filtering ~~~" << std::endl;
    for (size_t ix = 0; ix != iters; ix++) {
        std::cout << "\niteration " << ix << ", data = " << data << std::endl;
        auto value = infer.step(data);
        auto locs = value->template at<double>("loc").sample(rng, 100);
        std::cout << "mean posterior loc = " << mean_(*locs) << std::endl;
        auto scales = value->template at<double>("scale").sample(rng, 100);
        std::cout << "mean posterior scale = " << mean_(*scales) << std::endl;

        // random update
        auto sampled = value->sample_output(rng);
        data += Normal().sample(rng);
    }

    // inference machinery -- this time with a separate proposal distribution...
    trans.loc = 0;
    auto prop = normal_proposal(trans);
    q->clear();
    auto infer2 = filter<ImportanceSampling>(f, *q, trans, opts);

    // filter 2!
    std::cout << "\n\n~~~ using particle filtering with custom proposal ~~~" << std::endl;
    for (size_t ix = 0; ix != iters; ix++) {
        std::cout << "\niteration " << ix << ", data = " << data << std::endl;
        // note that we could pass a *different* proposal at each timestep if we chose!
        auto value = infer2.step(data, prop);
        auto locs = value->template at<double>("loc").sample(rng, 100);
        std::cout << "mean posterior loc = " << mean_(*locs) << std::endl;
        auto scales = value->template at<double>("scale").sample(rng, 100);
        std::cout << "mean posterior scale = " << mean_(*scales) << std::endl;        
        std::cout << "[alt computation] mean posterior loc = " << mean(value, "loc") << std::endl;
        std::cout << "[alt computation] mean posterior scale = " << mean(value, "scale") << std::endl;
        std::cout << "[alt computation] stddev posterior loc = " << stddev(value, "loc") << std::endl;
        std::cout << "[alt computation] stddev posterior scale = " << stddev(value, "scale") << std::endl;


        // random update
        auto sampled = value->sample_output(rng);
        data += Normal().sample(rng);
    }

    // inference machinery -- automated transition components
    auto opts25 = inf_options_t(1000, 100, 1000 + 100 * 100);
    auto trans25 = normal_trans_kernel_2();
    q->clear();
    auto infer25 = filter<AncestorMetropolis>(f, *q, trans25, opts25);

    std::cout << "\n\n~~~ using particle filtering with automated transition components ~~~" << std::endl;
    for (size_t ix = 0; ix != iters; ix++) {
        std::cout << "\niteration " << ix << ", data = " << data << std::endl;
        // note that we could pass a *different* proposal at each timestep if we chose!
        auto value = infer25.step(data);
        std::cout << "[alt computation] mean posterior loc = " << mean(value, "loc") << std::endl;
        std::cout << "[alt computation] mean posterior scale = " << mean(value, "scale") << std::endl;
        std::cout << "posterior score = " << score(value) << std::endl;
        std::cout << "AIC = " << aic(value) << std::endl;

        // random update
        auto sampled = value->sample_output(rng);
        data += Normal().sample(rng);
    }


    // inference machinery -- this time with metropolis approximate posterior
    trans.loc = 0;
    q->clear();
    size_t thin = 100;
    size_t burn = 1000;
    size_t num_pts = 100;
    auto opts2 = inf_options_t(thin, burn, burn + num_pts * thin);
    auto infer3 = filter<AncestorMetropolis>(f, *q, trans, opts2);

    // filter 3!
    std::cout << "\n\n~~~ using metropolis hastings particle filtering ~~~" << std::endl;
    for (size_t ix = 0; ix != iters; ix++) {
        std::cout << "\niteration " << ix << ", data = " << data << std::endl;
        // note that we could pass a *different* proposal at each timestep if we chose!
        auto value = infer3.step(data);
        auto locs = value->template at<double>("loc").sample(rng, 100);
        std::cout << "mean posterior loc = " << mean_(*locs) << std::endl;
        auto scales = value->template at<double>("scale").sample(rng, 100);
        std::cout << "mean posterior scale = " << mean_(*scales) << std::endl;
        // alternative way of computing these queries, saving memory
        std::cout << "[alt computation] mean posterior loc = " << mean(value, "loc") << std::endl;
        std::cout << "[alt computation] mean posterior scale = " << mean(value, "scale") << std::endl;
        std::cout << "[alt computation] stddev posterior loc = " << stddev(value, "loc") << std::endl;
        std::cout << "[alt computation] stddev posterior scale = " << stddev(value, "scale") << std::endl;
        std::cout << "posterior score = " << score(value) << std::endl;
        std::cout << "AIC = " << aic(value) << std::endl;

        // random update
        auto sampled = value->sample_output(rng);
        data += Normal().sample(rng);
    }
    
    return 0;
}


template<template<typename> class Policy>
void print_posterior(typed_map<Policy, double, Normal, Gamma>& m) {
    std::cout << "Loc dist is: " << m.template extract<Normal>("loc").string() << std::endl;
    std::cout << "Scale dist is: " << m.template extract<Gamma>("scale").string() << std::endl;
}

int test_update() {

    std::cout << "~~~ testing update functionality ~~~" << std::endl;

    // basic data structure for passing around distributions
    typed_map<DefaultPolicy, double, Normal, Gamma> m;
    m.insert_or_assign("obs", 5.0);
    m.insert_or_assign("loc", Normal(2.0, 2.0));
    m.insert_or_assign("scale", Gamma(2.0, 3.0));

    print_posterior(m);

    // do inference with updateable parameters
    upp_t<DefaultPolicy, double, double, Normal, Gamma> f = [](
        record_t<Normal, Gamma>& r, 
        typed_map<DefaultPolicy, double, Normal, Gamma>& input
    ) {
        auto loc = sample_u<Normal>(r, "loc", input, rng);
        auto scale = sample_u<Gamma>(r, "scale", input, rng);
        return observe_u<double>(r, "obs", Normal(loc, scale), input);
    };

    // draw a sample for sanity check
    record_t<Normal, Gamma> r;
    f(r, std::ref(m));
    std::cout << "Single record for sanity check:\n" << display(r) << std::endl;

    auto q = weighted_record<double, Normal, Gamma>();
    auto opts = inf_options_t(1000);
    auto infer = inference<LikelihoodWeighting>(f, *q, opts);

    std::cout << "doing inference with update-able model" << std::endl;
    auto res = infer(std::ref(m));

    auto updater = update<ParameterMatching, DefaultPolicy, decltype(res)>(f);
    auto new_m = updater(res);

    print_posterior(m);


    return 0;
}

template<template<typename> class Policy>
double my_model2(
    record_t<Normal, Parameter<non_negative<double>>>& r, 
    typed_map<Policy, double, Normal, Parameter<non_negative<double>>>& input
) {
    auto loc = sample_u<Normal>(r, "loc", input, rng);
    auto scale = param<non_negative<double>>(r, "scale")(1.0);
    return observe_u<double>(r, "obs", Normal(loc, scale), input);
}

template<template<typename> class Policy, typename... Ts>
void print_posterior(typed_map<Policy, double, Normal, Ts...>& m) {
    std::cout << "Loc dist is: " << m.template extract<Normal>("loc").string() << std::endl;
}

int test_update_filtering() {

    std::cout << "~~~ testing filtering with update, 'by hand' algo ~~~" << std::endl;

    // step 0
    using map1 = typed_map<DefaultPolicy, double, Normal, Parameter<non_negative<double>>>;

    map1 m;
    constexpr int num_iterations = 10;

    // real data
    auto noise = Normal(0.5, 0.5);
    double x = 3.0;
    
    // priors
    m.insert_or_assign("obs", x + noise.sample(rng));
    m.insert_or_assign("loc", Normal(2.0, 2.0));
    double last_obs = m.template extract<double>("obs");

    // invariants    
    upp_t<DefaultPolicy, double, double, Normal, Parameter<non_negative<double>>> f = my_model2<DefaultPolicy>;
    auto q = weighted_record<double, Normal, Parameter<non_negative<double>>>();
    auto opts = inf_options_t(1000);
    auto infer = inference<LikelihoodWeighting>(f, *q, opts);

    using res_t = decltype(std::declval<decltype(*q)>().emit());
    auto updater = update<ParameterMatching, DefaultPolicy, res_t>(f);
    
    for (int ix = 0; ix != num_iterations; ix++) {
        std::cout << "inference on iteration " << ix << std::endl;
        auto res = infer(std::ref(m));
        m = updater(res);
        /// \note very important to clear queryer cache!
        q->clear();

        // update with new observation
        std::cout << "obs = " << last_obs << std::endl;
        print_posterior(m);
        last_obs = last_obs + noise.sample(rng);
        m.insert_or_assign("obs", last_obs);
    }

    std::cout << "~~~ now, use built-in machinery ~~~" << std::endl;

    // above wrangling reduces to this
    auto algo = filter<ParameterMatching, DefaultPolicy>(f, *q, infer);

    for (int ix = 0; ix != num_iterations; ++ix) {

        // above wrangling reduces to this
        m = algo.step(m);

        std::cout << "obs = " << last_obs << std::endl;
        print_posterior(m);
        last_obs = last_obs + noise.sample(rng);
        m.insert_or_assign("obs", last_obs);
    }

    return 0;
}

template<template<typename> class Policy>
double my_model3(
    record_t<Normal, Gamma>& r, 
    typed_map<Policy, double, Normal, Gamma>& input
) {
    auto loc = sample_u<Normal>(r, "loc", input, rng);
    auto scale = sample_u<Gamma>(r, "scale", input, rng);
    return observe_u<double>(r, "obs", Normal(loc, scale), input);
}

int test_update_filtering_2() {
    std::cout << "~~~ comparing dist matching and normal approx ~~~" << std::endl;
    // use a distributional matching scheme and a normal approximation
    typed_map<DefaultPolicy, double, Normal, Gamma> m1;
    constexpr int num_iterations = 10;

    auto noise = Normal(0.5, 0.5);
    double x = 3.0;
    double last_obs = x = noise.sample(rng);
    
    // priors
    m1.insert_or_assign("obs", last_obs);
    m1.insert_or_assign("loc", Normal(2.0, 2.0));
    m1.insert_or_assign("scale", Gamma(3.0));

    std::cout << "~ dist matching ~" << std::endl;
    // default policy: invariants
    upp_t<DefaultPolicy, double, double, Normal, Gamma> f1 = my_model3<DefaultPolicy>;
    auto q = weighted_record<double, Normal, Gamma>();
    auto opts = inf_options_t(1000);
    auto infer1 = inference<LikelihoodWeighting>(f1, *q, opts);

    // default policy: inference
    auto algo1 = filter<ParameterMatching, DefaultPolicy>(f1, *q, infer1);
    for (int ix = 0; ix != num_iterations; ++ix) {
        m1 = algo1.step(m1);
        std::cout << "obs = " << last_obs << std::endl;
        print_posterior(m1);
        last_obs = last_obs + noise.sample(rng);
        m1.insert_or_assign("obs", last_obs);
    }

    ///
    q->clear();

    typed_map<NormalPolicy, double, Normal, Gamma> m2;
    m2.insert_or_assign("obs", last_obs);
    // same priors, but move to a normal approximation during iteration
    m2.insert_or_assign("loc", Normal(2.0, 2.0));
    m2.insert_or_assign("scale", Gamma(3.0));

    std::cout << "~ normal approx ~" << std::endl;
    // normal policy: invariants
    upp_t<NormalPolicy, double, double, Normal, Gamma> f2 = my_model3<NormalPolicy>;
    auto infer2 = inference<LikelihoodWeighting>(f2, *q, opts);

    // default policy: inference
    auto algo2 = filter<ParameterMatching, NormalPolicy>(f2, *q, infer2);
    for (int ix = 0; ix != num_iterations; ++ix) {
        m2 = algo2.step(m2);
        std::cout << "obs = " << last_obs << std::endl;
        print_posterior(m2);
        last_obs = last_obs + noise.sample(rng);
        m2.insert_or_assign("obs", last_obs);
    }

    return 0;
}

int main(int argc, char ** argv) {
    auto status = std::vector<int>({
        test_filter_basic(),
        test_update(),
        test_update_filtering(),
        test_update_filtering_2()
    });
    if (*std::max_element(status.begin(), status.end()) > 0) {
        std::cout << "~~~ Test failed; read log for more. ~~~\n";
        return 1;
    } else {
        std::cout << "~~~ All tests passed. ~~~\n";
        return 0;
    }
}
