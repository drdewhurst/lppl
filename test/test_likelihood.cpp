/* 
 * This file is part of lppl.
 * Copyright David Rushing Dewhurst, 2022 - present.
 * Released under MIT license
 */

#include <distributions.hpp>
#include <record.hpp>
#include <inference/inference.hpp>
#include <inference/importance/likelihood.hpp>
#include <plate.hpp>
#include <query.hpp>

#include <cmath>
#include <functional>
#include <iostream>
#include <limits>
#include <random>
#include <string>
#include <vector>

using namespace Distributions;


std::minstd_rand rng(202);


double normal_model(record_t<Normal>& r, double obs_val) {
    auto loc = sample(r, "loc", Normal(0.0, 4.0), rng);
    auto log_scale = sample(r, "log_scale", Normal(0.0, 1.0), rng);
    observe(r, "obs", Normal(loc, std::exp(log_scale)), obs_val);
    return loc;
}


int test_lw_mean() {
    auto q = weighted_mean<double, Normal>("loc");
    auto opts = inf_options_t{2000};
    double obs = -3.0;
    std::function<double(record_t<Normal>&, double)> f = normal_model;
    auto infer = inference<LikelihoodWeighting>(f, *q, opts);
    auto res = infer(obs);
    std::cout << "E[loc] = " << res << std::endl;
    return 0;
}

// int test_lw_multiple_mean() {
//     auto q = QueryerCollection<double, double, WeightedMean, Normal>({"loc", "log_scale"});
//     auto opts = inf_options_t(2000);
//     double obs = -3.0;
//     std::function<double(record_t<Normal>&, double)> f = normal_model;
//     auto infer = inference<LikelihoodWeighting>(f, q, opts);
//     auto post_means = infer(obs);
//     std::cout << "Posterior means = " << std::endl;
//     for (const auto& [k, v] : *post_means) {
//         std::cout << k << " = " << v << std::endl;
//     }
//     return 0;
// }

int test_lw_evidence() {
    // compute the evidence (marginal probability of data under the model)
    // two ways to do this, demo both
    auto q = weighted_record<double, Normal>();
    auto opts = inf_options_t(2000);
    double obs = -3.0;
    pp_t<double, double, Normal> f = normal_model;
    auto infer = inference<LikelihoodWeighting>(f, *q, opts);
    auto res = infer(obs);

    // compute directly from weights in queryer
    // -log N + logsumexp_n(w_n)
    double log_evidence = -1.0 * std::log(res->size()) + q->lse_weights();

    std::cout << "log p(x) = " << log_evidence <<  std::endl;

    // maximum likelihood estimate
    auto q_mle = Optimizer<record_t<Normal>, double, Normal>(
        [](record_t<Normal>& r, double& output, double& weight) { return r; },
        [](record_t<Normal>& r, double& output, double& weight){ return loglikelihood(r); }
    );
    auto infer_mle = inference<LikelihoodWeighting>(f, q_mle, opts);
    auto res_mle = infer_mle(obs);
    std::cout << "MLE estimate = " << display(res_mle) << std::endl;

    // maximum a posteriori estimate
    auto q_map = Optimizer<record_t<Normal>, double, Normal>(
        [](record_t<Normal>& r, double& output, double& weight) { return r; },
        [](record_t<Normal>& r, double& output, double& weight){ return logprob(r); }
    );
    auto infer_map = inference<LikelihoodWeighting>(f, q_map, opts);
    auto res_map = infer_map(obs);
    std::cout << "MAP estimate = " << display(res_map) << std::endl;

    return 0;
}

double normal_mixture(record_t<Normal, Gamma, Categorical>& r, double data) {
    auto choice = sample(r, "choice", Categorical(), rng);
    double loc, scale;
    if (choice) {
        loc = sample(r, "+loc", Normal(1.0), rng);
        scale = sample(r, "+scale", Gamma(), rng);
    } else {
        loc = sample(r, "-loc", Normal(-2.0), rng);
        scale = sample(r, "-scale", Gamma(2.0, 0.5), rng);
    }
    return observe(r, "data", Normal(loc, scale), data);
}

// int test_lw_multiple_mean_mixture() {
//     auto q = QueryerCollection<double, double, WeightedMean, Normal, Gamma, Categorical>(
//         {"+loc", "-loc", "+scale", "-scale"}
//     );
//     auto opts = inf_options_t(2000);
//     double obs = -1.5;
//     std::function<double(record_t<Normal, Gamma, Categorical>&, double)> f = normal_mixture;
//     auto infer = inference<LikelihoodWeighting>(f, q, opts);
//     auto post_means = infer(obs);
//     std::cout << "Posterior means = " << std::endl;
//     for (const auto& [k, v] : *post_means) {
//         std::cout << k << " = " << v << std::endl;
//     }
//     return 0;
// }

int test_lw_mixture_records() {

    auto q = weighted_record<double, Normal, Gamma, Categorical>();
    auto opts = inf_options_t(5);
    double obs = -1.5;
    std::function<double(record_t<Normal, Gamma, Categorical>&, double)> f = normal_mixture;
    auto infer = inference<LikelihoodWeighting>(f, *q, opts);
    auto some_records = infer(obs);
    std::cout << "Generated " << opts._num_iterations << " records:" << std::endl;
    for (auto& r : some_records->_v) {
        std::cout << display(r) << std::endl;
    }
    auto r_samp = some_records->sample(rng);
    std::cout << "Sampled record:\n" << display(*r_samp) << std::endl;
    return 0;
}

template<size_t N>
double var_dice_model(record_t<Normal, static_plate<Categorical, N>>& r, double obs_value) {
    auto dice_values = sample(r, "dice_values", static_plate<Categorical, N>(6), rng);
    double value = 0.0;
    for (size_t ix = 0; ix != N; ix++) {
        value += sample(r, "payoff " + std::to_string(ix), Normal(0.0, 1 + dice_values->at(ix)), rng);
    }
    return observe(r, "value", Normal(value, 1.0), obs_value);
}

int test_plate_inference() {
    constexpr size_t dim = 4;
    auto q = weighted_mean<double, Normal, static_plate<Categorical, dim>>("payoff 2");
    auto opts = inf_options_t(2000);
    double obs_value = -3.0;  // bad luck!
    pp_t<double, double, Normal, static_plate<Categorical, dim>> f = var_dice_model<dim>;
    auto infer = inference<LikelihoodWeighting>(f, *q, opts);
    auto post_mean = infer(obs_value);
    std::cout << "posterior mean of payoff from dice 2 is " << post_mean << std::endl;

    return 0;
}


int main(int argc, char ** argv) {
    auto status = std::vector<int>({
        test_lw_mean(),
        // test_lw_multiple_mean(),
        test_lw_evidence(),
        // test_lw_multiple_mean_mixture(),
        test_lw_mixture_records(),
        test_plate_inference()
    });
    if (*std::max_element(status.begin(), status.end()) > 0) {
        std::cout << "~~~ Test failed; read log for more. ~~~\n";
        return 1;
    } else {
        std::cout << "~~~ All tests passed. ~~~\n";
        return 0;
    }
}